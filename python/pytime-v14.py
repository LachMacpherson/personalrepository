import numpy as np
import matplotlib as mpl
#mpl.use('Agg')
import matplotlib.pyplot as plt
import xlrd
import os
from datetime import datetime
from datetime import timedelta
import operator
import csv
import re
import sys


__author__ = "Andrew Gillies and Lachlan MacPherson"
__copyright__ = "Copyright (c) 2014-2015, Alstom Grid UK Limited"
__license__ = "See Included License Text File"
__version__ = "0.11"

def enum(**enums):
    return type('Enum', (), enums)

TENDER_COSTCENTER = '3415-YS010'

DEBUG = False
IGNORE_INVALID_TIMESHEET_VERSION = True

""""RndCodeMap = {"3415P5000-06.1.3.2.1.1":"3415R5014.04",
              "3415P5000-06.1.3.3.1.4":"3415R5014.05",
              "3415P5000-06.1.3.2.1.6":"3415R5014.02",
              "3415P5000-06.1.3.3.1.3":"3415R5014.02",
              "3415P5000-06.1.3.2.1.2":"3415R5014.06",
              "3415P5000-06.1.3.3.1.1":"3415R5014.06",
              "3415P5000-06.1.3.2.1.3":"3415R5014.07",
              "3415P5000-06.1.3.3.1.2":"3415R5014.07"}
"""
RndCodeMap = {}
WINDOWS_GROUPS = { "dev" : { "path" : "k:\Development", "approved" : "k:\Development\Approved"},
           "ops" : { "path" : "k:\Operations", "approved" : "k:\Operations\Approved" },
           "pms" : { "path" : "k:\Operations", "approved" : "k:\Operations\Approved" },
           "pst" : { "path" : "k:\PowerSystems", "approved" : "k:\PowerSystems\Approved" } }

LINUX_GROUPS = { "dev" : { "path" : "/mnt/timetracking/Development", "approved" : "/mnt/timetracking/Development/Approved"},
           "ops" : { "path" : "/mnt/timetracking/Operations", "approved" : "/mnt/timetracking/Operations/Approved" },
           "pms" : { "path" : "/mnt/timetracking/Operations" , "approved" : "/mnt/timetracking/Operations/Approved"},
           "pst" : { "path" : "/mnt/timetracking/PowerSystems/Approved" , "approved" : "/mnt/timetracking/PowerSystems/Approved"} }

STAFF = {
    "Alan Holmes" : {"group" : "ops",
                        "number" : 34151042,
                        "standard_hrs" : 37.5,
                        "cat_nonchargeable" : "GCBTCI" ,
                        "cat_chargeable" : "GCBTCU",
                        "roles" : ["SE","PE"] },
    "Alan West" : { "group" : "dev",
                           "number" : 34151039,
                           "startdate": '23/11/2015',
                           "standard_hrs" : 37.5,
                           "cat_nonchargeable" : "GCBTCG" ,
                           "cat_chargeable" : "GCBTCS",
                           "roles" : ["DV"] },   
    "Andreas Glatz" : { "group" : "dev",
                        "number" : 34151031,
                        "standard_hrs" : 37.5,
                        "cat_nonchargeable" : "GCBTCG" ,
                        "cat_chargeable" : "GCBTCS",
                        "roles" : ["DV"] }, 
    "Andrew Aspinall" : {"group" : "ops",
                         "number" : 34151003,
                         "standard_hrs" : 37.5,
                         "cat_nonchargeable" : "GCBTCI" ,
                         "cat_chargeable" : "GCBTCU",
                         "roles" : ["SE","PM","PE"] },         
    "Andrew Gillies" : {"group" : "ops",
                        "number" : 34151007,
                        "standard_hrs" : 37.5,
                        "cat_nonchargeable" : "GCBTCI" ,
                        "cat_chargeable" : "GCBTCU",
                        "roles" : ["SE","PM","PE"] },
    "Brian Berry" : { "group" : "pst",
                            "number" : 34151036,
                            "startdate" : '3/8/2015',
                            "standard_hrs" : 37.5,
                            "cat_nonchargeable" : "GCBTCH" ,
                            "cat_chargeable" : "GCBTCT",
                        "roles" : ["PS", "TE"] },        
    "Chris Lisle" : { "group" : "dev",
                      "number" : 34151011,
                      "standard_hrs" : 37.5,
                      "cat_nonchargeable" : "GCBTCG" ,
                      "cat_chargeable" : "GCBTCS",
                      "roles" : ["DV"] },
    "Christos Takoudis" : { "group" : "pst",
                            "number" : 34151032,
                            "standard_hrs" : 37.5,
                            "cat_nonchargeable" : "GCBTCH" ,
                            "cat_chargeable" : "GCBTCT",
                        "roles" : ["PS", "TE"] },                     
   "Declan McNulty" : {"group" : "ops",
                        "number" : 34151017,
                        "standard_hrs" : 37.5,
                        "cat_nonchargeable" : "GCBTCI" ,
                        "cat_chargeable" : "GCBTCU",
                        "roles" : ["SE","PE"] },
   "Deepak Singh" : { "group" : "dev",
                         "number" : 34151038,
                         "startdate": '2/11/2015',
                         "standard_hrs" : 37.5,
                          "cat_nonchargeable" : "GCBTCG" ,
                          "cat_chargeable" : "GCBTCS",
                          "roles" : ["TE"] },        
##   "Douglas Wilson" : { "group" : "pst",
##                         "number" : 34151023,
##                         "standard_hrs" : 37.5,
##                         "cat_nonchargeable" : "GCBTCH" ,
##                         "cat_chargeable" : "GCBTCT",
##                        "roles" : ["PS"]}, 
    "Edwin Pauwels" : {"group" : "pms",
                       "number" : 34151034,
                       "standard_hrs" : 37.5,
                       "cat_nonchargeable" : "GCBTCJ" ,
                       "cat_chargeable" : "GCBTCW",
                       "roles" : ["PM"] },
    "Geoff Eddy" : { "group" : "dev",
                     "number" : 34151006,
                     "standard_hrs" : 37.5,
                     "cat_nonchargeable" : "GCBTCG" ,
                     "cat_chargeable" : "GCBTCS",
                     "roles" : ["DV"] },                     
    "Iain Macquarie" : { "group" : "dev",
                         "number" : 34151013,
                         "standard_hrs" : 37.5,
                         "cat_nonchargeable" : "GCBTCG" ,
                         "cat_chargeable" : "GCBTCS",
                         "roles" : ["DV"] },
    "Ivan Martin" : {"group" : "ops",
                     "number" : 34151015,
                     "standard_hrs" : 37.5,
                     "cat_nonchargeable" : "GCBTCJ" ,
                     "cat_chargeable" : "GCBTCW",
                     "roles" : ["SE","PM","PE"] },
    "Jamie Gilchrist" : {"group" : "ops",
                        "number" : 34151035,
                        "startdate": '20/4/2015',
                        "standard_hrs" : 37.5,
                        "cat_nonchargeable" : "GCBTCI" ,
                        "cat_chargeable" : "GCBTCU",
                        "roles" : ["SE","PE"] },
    "Jacques Warichet" : { "group" : "pst",
                           "number" : 34151033,
                           "standard_hrs" : 37.5,
                           "cat_nonchargeable" : "GCBTCH" ,
                           "cat_chargeable" : "GCBTCT",
                           "roles" : ["PS"] },    
    "John Hawthorne" : { "group" : "dev",
                         "number" : 34151008,
                         "standard_hrs" : 37.5,
                         "cat_nonchargeable" : "GCBTCG" ,
                         "cat_chargeable" : "GCBTCS",
                        "roles" : ["DV"] },
    "Karine Hay" : { "group" : "pst",
                     "number" : 34151009,
                     "standard_hrs" : 30,
                     "cat_nonchargeable" : "GCBTCH" ,
                     "cat_chargeable" : "GCBTCT",
                     "roles" : ["PS"] },
 ##   "Kyriaki Maleka" : { "group" : "pst",
 ##                        "number" : 34151030,
 ##                        "standard_hrs" : 37.5,
 ##                        "cat_nonchargeable" : "GCBTCH" ,
 ##                        "cat_chargeable" : "GCBTCT",
 ##                        "roles" : ["PS"] },
    "Lahrasub Siraj" : { "group" : "dev",
                           "number" : 34151037,                           
                           "startdate": '24/8/2015',
                           "standard_hrs" : 37.5,
                           "cat_nonchargeable" : "GCBTCG" ,
                           "cat_chargeable" : "GCBTCS",
                           "roles" : ["DV"] },            
    "Ljiljana Spadavecchia" : { "group" : "dev",
                                "number" : 34151021,
                                "standard_hrs" : 37.5,
                                "cat_nonchargeable" : "GCBTCG" ,
                                "cat_chargeable" : "GCBTCS",
                                "roles" : ["DV"] },                     
    "Lukasz Kowalski" : { "group" : "dev",
                          "number" : 34151026,
                          "standard_hrs" : 37.5,
                          "cat_nonchargeable" : "GCBTCG" ,
                          "cat_chargeable" : "GCBTCS",
                          "roles" : ["TE"] },                        
    "Mark Anderson" : { "group" : "dev",
                        "number" : 34151002,
                        "standard_hrs" : 37.5,
                        "cat_nonchargeable" : "GCBTCG" ,
                        "cat_chargeable" : "GCBTCS",
                        "roles" : ["DV"] },
##    "Mike Smith" : { "group" : "dev",
##                    "number" : 34151020,
##                     "standard_hrs" : 30,
##                     "cat_nonchargeable" : "GCBTCG" ,
##                     "cat_chargeable" : "GCBTCS",
##                     "roles" : ["DV"] },
    "Natheer al-Ashwal" : { "group" : "pst",
                            "number" : 34151001,
                            "standard_hrs" : 37.5,
                            "cat_nonchargeable" : "GCBTCH" ,
                            "cat_chargeable" : "GCBTCT",
                            "roles" : ["PS"] },                                              
    "Neil French" : { "group" : "dev",
                      "number" : 34151024,
                      "standard_hrs" : 37.5,
                      "cat_nonchargeable" : "GCBTCG" ,
                      "cat_chargeable" : "GCBTCS",
                      "roles" : ["TE"] },                             
    "Neil Johnston" : { "group" : "dev",
                        "number" : 34151027,
                        "standard_hrs" : 37.5,
                        "cat_nonchargeable" : "GCBTCG" ,
                        "cat_chargeable" : "GCBTCS",
                        "roles" : ["TE"] },
    "Patrick McNabb" : { "group" : "pst",
                         "number" : 34151016,
                         "standard_hrs" : 37.5,
                         "cat_nonchargeable" : "GCBTCH" ,
                         "cat_chargeable" : "GCBTCT",
                        "roles" : ["PS"] },
    "Peter Gauld" : { "group" : "dev",
                         "number" : 34151040,
                         "startdate": '14/3/2016',
                         "standard_hrs" : 37.5,
                         "cat_nonchargeable" : "GCBTCG" ,
                         "cat_chargeable" : "GCBTCS",
                        "roles" : ["DV"] },
    "Richard Davey" : {"group" : "pms",
                       "number" : 34151005,
                       "standard_hrs" : 37.5,
                       "cat_nonchargeable" : "GCBTCJ" ,
                       "cat_chargeable" : "GCBTCW",
                       "roles" : ["PM"] },
##    "Ricardo Lira" : { "group" : "pst",
##                       "number" : 34151010,
##                       "standard_hrs" : 37.5,
##                       "cat_nonchargeable" : "GCBTCH" ,
##                       "cat_chargeable" : "GCBTCT",
##                        "roles" : ["PS"] }, 
    "Sean Norris" : { "group" : "pst",
                      "number" : 34151018,
                      "standard_hrs" : 37.5,
                      "cat_nonchargeable" : "GCBTCH" ,
                      "cat_chargeable" : "GCBTCT",
                      "roles" : ["PS"] },                                              
    "Stanley Fernandes" : { "group" : "dev",
                            "number" : 34151029,
                            "standard_hrs" : 37.5,
                            "cat_nonchargeable" : "GCBTCG" ,
                            "cat_chargeable" : "GCBTCS",
                            "roles" : ["DV"] },
    "Stuart Clark" : { "group" : "pst",
                       "number" : 34151004,
                       "standard_hrs" : 37.5,
                       "cat_nonchargeable" : "GCBTCH" ,
                       "cat_chargeable" : "GCBTCT",
                        "roles" : ["PS"] },
    "Vedran Peric" : { "group" : "pst",
                       "number" : 34151041 ,
                       "startdate": '4/4/2016',
                       "standard_hrs" : 37.5,
                       "cat_nonchargeable" : "GCBTCH" ,
                       "cat_chargeable" : "GCBTCT",
                        "roles" : ["PS"] },          
 }


##SAP = [ 'Jan-2016-20-06-to-03-08.xlsx' ]
SAP = []
Problems = []

ProblemKeys = enum(OVER_ALLOCATED='Over Allocated', UNDER_ALLOCATED='Under Allocated', INVALID_VERSION='Invalid Timesheet Version',NOT_PERMITTED='Not Permitted to use this code',NO_ROLES='User has no roles defined')



class Problem():
    def __init__(self, person, sheet, key, value):
        self.person = person
        self.key = key
        self.sheet = sheet
        self.value = value
        
class TimeSheet():

    def __init__(self,person,info):
        self.person = person
        self.date = info
        self.cat_nonchargeable = info["cat_nonchargeable"]
        self.cat_chargeable = info["cat_chargeable"]
        self.number = info["number"]
        self.group = info["group"]
        self.standard_hrs = info["standard_hrs"]
        self.codenames = {}
        self.template = {}

        self.style = { 'original': { 'days' : [5,6,7,8,9],
                                     'code' : 3,
                                     'desc' : 1,
                                     'sheet' : u'Timesheet',
                                     'start' : 4,
                                     'category' : 0,
                                     'required': 0},
                       'old': { 'days' : [3,4,5,6,7],
                                'code' : 0,
                                'desc' : 1,
                                'sheet' : u'URTDSM',
                                'start' : 4,
                                'category' : 0,
                                'required': 0},
                       'new1': { 'days' : [3,4,5,6,7],
                                 'code' : 1,
                                 'desc' : 0,
                                 'sheet' : u'Timesheet',
                                 'start' : 4,
                                 'category' : 0,
                                 'required': 0},
                       'new2': { 'days' : [4,5,6,7,8],
                                 'code' : 2,
                                 'desc' : 1,
                                 'sheet' : u'Timesheet',
                                 'start' : 6,
                                 'category' : 0,
                                 'required': 1}
                   }

        self.admin_codes = [333415500002,u'333415500002',u'PSY - Departmental Administration']
        self.it_codes = [333415500007,u'333415500007',u'PSY - Internal IT'] 
        self.tender_codes = [333415500006,u'333415500006',u'PSY - Tendering Support']
        self.training_codes = [333415500005,u'333415500005',u'PSY - Training']
        self.holiday_codes = [333415500001,u'333415500001',u'PSY - Holiday']
        self.sick_codes = [333415500003,u'333415500003',u'PSY - Sick leave']
        self.absent_codes = [333415500004,u'333415500004',u'PSY - Authorised Absence']
        self.offwork_codes = [333415500001,333415500003,333415500004,u'333415500001',u'333415500003',u'333415500004',u'PSY - Holiday',u'PSY - Sick leave',u'PSY - Authorised Absence'] # holiday, sick, authoirsed absence
        
        self.days = []
        self.sheets = []
        self.hours = []
        self.days5 = []
        self.hours5 = []



        self.transform = {'holiday' : '333415500001', 'admin' : '333415500002', 'sick' : '333415500003', 'absent' : '333415500004', 'training' : '333415500005', 'tender' : '333415500006', 'it' : '333415500007'}
        self.exclude = ['total', 'offwork']


        self.missing = { 'getco' : "3415P5042-06.01" }

        self.SAPhours = {}

    def next_weekday(self, d, weekday):
        days_ahead = weekday - d.weekday()
        if days_ahead <= 0: # Target day already happened this week
            days_ahead += 7
        return d + timedelta(days_ahead)


    def getdates(self,fivedays=True):
        # merge SAP dates with excel dates
        sapdates = self.getdatesSAP(fivedays=fivedays)
        wsdates = self.getdatesWS(fivedays=fivedays)
        
        return sorted(list(set(sapdates+wsdates)))

    def getdatabydate(self,date,fd,sap=True,report=True, decimalType='.'):
        req_date = datetime.strptime(date, '%Y%m%d')

        projectresWS = {}
        adminresWS = {}

        projectresSAP = {}
        adminresSAP = {}


        # Sanity check, this is a Monday?
        if req_date.weekday()==0:
            for d in range(0,5):
                for i in range(0,len(self.days)):
                    if self.days[i]==req_date:

                        for k,j in self.hours[i].iteritems():
                            if j>0.0:
                                if k in self.exclude:
                                    continue
                                if k in self.transform:
                                    newkey = self.transform[k]
                                    if not adminresWS.has_key(newkey):
                                        adminresWS[newkey] = [0.0]*5
                                    adminresWS[newkey][d] = j
                                else:
                                    if not projectresWS.has_key(k):
                                        projectresWS[k] = [0.0]*5
                                    projectresWS[k][d] = j
                                    
                    if self.SAPhours.has_key(req_date):
                        for k,j in self.SAPhours[req_date].iteritems():
                            if j>0.0:
                                if k in self.exclude:
                                    continue
                                if k in self.transform:
                                    newkey = self.transform[k]
                                    if not adminresSAP.has_key(newkey):
                                        adminresSAP[newkey] = [0.0]*5
                                    adminresSAP[newkey][d] = j
                                else:
                                    if not projectresSAP.has_key(k):
                                        projectresSAP[k] = [0.0]*5
                                    projectresSAP[k][d] = j

                req_date = req_date+timedelta(days=1)
        else:
            print("NOTE, was not given a Monday")


        
        # reporting
        if report:
            totalWS = 0.0
            for c,h in projectresWS.iteritems():
                totalWS += sum(h)

            for c,h in adminresWS.iteritems():
                totalWS += sum(h)        

            totalSAP = 0.0
            for c,h in projectresSAP.iteritems():
                totalSAP += sum(h)

            for c,h in adminresSAP.iteritems():
                totalSAP += sum(h)        

            reportcols = [ '', totalSAP, totalWS, '']
            blankcols = [ '', '', '', '']

            if len(projectresWS)==0 and len(adminresWS)==0:
                #reportcols[2] = "No WS"
                reportcols[2] = ""

            if len(projectresSAP)==0 and len(adminresSAP)==0:
                #reportcols[1] = "No SAP"
                reportcols[1] = ""
            else:
                for c,h in projectresWS.iteritems():
                    if not projectresSAP.has_key(c):
                        reportcols[3] = reportcols[3] + " %s (%.1f) missing from SAP;" %(c,sum(h))
                    else:
                        if sum(h) != sum(projectresSAP[c]):
                            reportcols[3] = reportcols[3] + " %s diff (%.1f SAP vs %.1f WS);" %(c,sum(projectresSAP[c]),sum(h))

                for c,h in adminresWS.iteritems():
                    if not adminresSAP.has_key(c):
                        reportcols[3] = reportcols[3] + " %s (%.1f) missing from SAP;" %(c,sum(h))
                    else:
                        if sum(h) != sum(adminresSAP[c]):
                            reportcols[3] = reportcols[3] + " %s diff (%.1f SAP vs %.1f WS);" %(c,sum(adminresSAP[c]),sum(h))

        else:
            reportcols = []
            blankcols = []

        total = 0
        projectres = {}
        if len(projectresWS)==0:
            projectres = projectresSAP
        else:
            if len(projectresSAP)>0:
                if sap:
                    print("Have both SAP and WS for %s, using SAP" %date)
                    projectres = projectresSAP
                else:
                    print("Have both SAP and WS for %s, using WS" %date)
                    projectres = projectresWS
            else:
                projectres = projectresWS 

        adminres = {}
        if len(adminresWS)==0:
            adminres = adminresSAP
        else:
            if len(adminresSAP)>0:
                if sap:
                    adminres = adminresSAP
                else:
                    adminres = adminresWS
            else:
                adminres = adminresWS

        # write to CSV

        week_date = datetime.strptime(date, '%Y%m%d')
        fd.writerow([ '%s %s' %(self.person,week_date.strftime('%d/%m/%Y'))] + [self.number] + [''] + 7*[''] + reportcols)
        for c,h in projectres.iteritems():

            fd.writerow([c] + [''] + ['']+ [''] + ["=\"333415500000\""] + ['']+ self.convertDecimalType(decimalType,h) + blankcols) 
            total += sum(h)

        for c,h in adminres.iteritems():
            fd.writerow([' '] + [''] + [''] + ['%s' %self.mapCostCenter(c) ] + ["=\"%s\"" %c]+ [''] + self.convertDecimalType(decimalType,h) + blankcols)
               
            total += sum(h)

        #if total!=37.5:
        #    print("WARNING, week %s does not add to 37.5 hours (%d)" %(week_date.strftime('%d/%m/%Y'),total))

    def mapCostCenter(self, code):
        
        cost_center = self.cat_chargeable
        if (code in self.offwork_codes):
            cost_center = self.cat_nonchargeable
        elif (code in self.tender_codes):
            cost_center = TENDER_COSTCENTER
        return cost_center
    

    def convertDecimalType(self, decType, h):
        decmark_reg = re.compile('(?<=\d)\.(?=\d)')
        hours = []
        if (decType != '.'):
            for hour in h:
                hr = decmark_reg.sub(decType, str(hour))
                hours.append(hr)
        else:
            hours = h
        return hours

    def getdata(self,dataname,fivedays=True):
        insap = []

        sapdates = self.getdatesSAP(fivedays=fivedays)
        sapdata = self.getdataSAP(dataname,fivedays=fivedays)

        wsdates = self.getdatesWS(fivedays=fivedays)
        wsdata = self.getdataWS(dataname,fivedays=fivedays)

        sap = dict(zip(sapdates,sapdata))
        ws = dict(zip(wsdates,wsdata))
        
        alldates = sorted(list(set(sapdates+wsdates)))

        totals = []
        
        for d in alldates:
            if sap.has_key(d):
                if ws.has_key(d):
                    if sap[d]==ws[d]:
                        totals.append(sap[d])
                        insap.append(True)
                    else:
                        print('%s %s SAP,WS DIFFERENCE: %s SAP %.2f  WS %.2f' %(self.person,d.strftime('%d/%m/%Y'),dataname,sap[d],ws[d]))
                        totals.append(max(sap[d],ws[d]))
                        if sap[d]<0.5:
                            insap.append(False)
                        else:
                            insap.append(True)
                else:
                    totals.append(sap[d])
                    insap.append(True)
            else:
                insap.append(False)
                if ws.has_key(d):
                    totals.append(ws[d])
                else:
                    print("PROBLEM - data inconsistency")

        return (totals,insap)
        


    def getdatesSAP(self,fivedays=False):
        alldates = sorted(self.SAPhours.keys())

        if len(alldates)==0:
            return alldates

        if fivedays:
            retdates = []
            # find earliest day
            startday = alldates[0]
            # find first Monday after start date
            next_monday = self.next_weekday(startday, 0) # 0 = Monday, 1=Tuesday, 2=Wednesday...
            if (next_monday-startday) > timedelta(days=2):
                first_monday = next_monday-timedelta(days=7)
            else:
                first_monday = next_monday
            while first_monday<alldates[len(alldates)-1]:
                retdates.append(first_monday)
                first_monday = first_monday+timedelta(days=7)
            return retdates
        else:
            return alldates


    def getdataSAP(self,dataname,fivedays=False):
        totals = []

        if len(sorted(self.SAPhours.keys()))==0:
            return totals
        
        if fivedays:
            alldates = sorted(self.SAPhours.keys())
            # find earliest day
            startday = alldates[0]
            # find first Monday after start date
            next_monday = self.next_weekday(startday, 0) # 0 = Monday, 1=Tuesday, 2=Wednesday...
            if (next_monday-startday) > timedelta(days=2):
                first_monday = next_monday-timedelta(days=7)
            else:
                first_monday = next_monday
            while first_monday<alldates[len(alldates)-1]:
                total = 0.0
                for d,h in sorted(self.SAPhours.iteritems(), key=operator.itemgetter(0)):
                    if h.has_key(dataname):
                        if (d>=first_monday) and (d<first_monday+timedelta(days=7)):
                            total+=h[dataname]
                totals.append(total)
                first_monday = first_monday+timedelta(days=7)            
        else:
            for d,h in sorted(self.SAPhours.iteritems(), key=operator.itemgetter(0)):
                if h.has_key(dataname):
                    #print("found in %s" %self.person)
                    totals.append(h[dataname])
                else:
                    totals.append(0.0)

        return totals

    def getdatesWS(self,fivedays=True):
        if fivedays:
            return self.days5
        else:
            return self.days


    def getdataWS(self,dataname,fivedays=True):
        totals = []
        if fivedays:
            hours = self.hours5
        else:
            hours = self.hours
        for h in hours:
            if h.has_key(dataname):
                #print("found in %s" %self.person)
                totals.append(h[dataname])
            else:
                totals.append(0.0)

        return totals

    def getProjectsWS(self):
        codes = []
        for h in self.hours:
            for k,v in h.iteritems():
                if k in codes:
                    pass
                else:
                    if k.find('3415')==0:
                        codes.append(k)
        return codes

    def getnonProjectsWS(self):
        codes = []
        for h in self.hours:
            for k,v in h.iteritems():
                if k in codes:
                    pass
                else:
                    if k.find('3415')!=0:
                        if not (k in self.exclude):
                            codes.append(k)
        return codes


    def addWorkSheet(self,date,wsname,worksheet,style,version,template,codenames,units=1):

        ws_date = datetime.strptime(date, '%Y%m%d')
        ws_dates = [ws_date,ws_date+timedelta(days=1),ws_date+timedelta(days=2),ws_date+timedelta(days=3),ws_date+timedelta(days=4)]
        # ensure whole sheets adds to 37.5
        ws_style = self.style[style]
        num_rows = worksheet.nrows - 1
        curr_row = -1
        hours = {'total': 0.0, 'admin':0.0, 'it':0.0, 'tender':0.0, 'offwork':0.0, 'holiday':0.0, 'sick':0.0, 'absent':0.0, 'training':0.0}
        week_hours = [{'total': 0.0, 'admin':0.0, 'it':0.0, 'tender':0.0, 'offwork':0.0, 'holiday':0.0, 'sick':0.0, 'absent':0.0, 'training':0.0},{'total': 0.0, 'admin':0.0, 'it':0.0, 'tender':0.0, 'offwork':0.0, 'holiday':0.0, 'sick':0.0, 'absent':0.0, 'training':0.0},{'total': 0.0, 'admin':0.0, 'it':0.0, 'tender':0.0, 'offwork':0.0, 'holiday':0.0, 'sick':0.0, 'absent':0.0, 'training':0.0},{'total': 0.0, 'admin':0.0, 'it':0.0, 'tender':0.0, 'offwork':0.0, 'holiday':0.0, 'sick':0.0, 'absent':0.0, 'training':0.0},{'total': 0.0, 'admin':0.0, 'it':0.0, 'tender':0.0, 'offwork':0.0, 'holiday':0.0, 'sick':0.0, 'absent':0.0, 'training':0.0}]
        
        required = ws_style['required']
        code = ws_style['code']

        if style != "new1" and style != "new2":
            #ignore version check if old format
            internal_version = version
        else:
            internal_version = worksheet.cell_value(3, 1)

        if isinstance(internal_version, float) or isinstance(internal_version, str):
            if isinstance(internal_version, str) or int(internal_version) != int(version):
                Problems.append(Problem(self.person, wsname, ProblemKeys.INVALID_VERSION, internal_version))
            
        while curr_row < num_rows:
            curr_row += 1
            if curr_row>=ws_style['start']:
                # Cell Types: 0=Empty, 1=Text, 2=Number, 3=Date, 4=Boolean, 5=Error, 6=Blank
                if worksheet.cell_type(curr_row, required)!=0: 
                    # only include lines that have the required fild not empty
                    iday = 0
                    for day in ws_style['days']:
                        if worksheet.cell_type(curr_row, day)!=0:
                            try:
                                h = worksheet.cell_value(curr_row, day)/units
                            except:
                                if DEBUG:
                                    print("PROBLEM: was expecting this [%s:%s] to be a number \"%s\" in WS %s" %(curr_row, day, worksheet.cell_value(curr_row, day), wsname))
                                h=0
                            #print worksheet.row(curr_row)
                            hours['total'] += h
                            week_hours[iday]['total'] += h
                            acode = worksheet.cell_value(curr_row, code)
                            overhead = False
                            if RndCodeMap.has_key(acode):
                                acode = RndCodeMap[acode]
                            if acode in self.admin_codes:
                                hours['admin'] += h
                                week_hours[iday]['admin'] += h
                                overhead = True
                            if acode in self.it_codes:
                                hours['it'] += h
                                week_hours[iday]['it'] += h
                                overhead = True
                            if acode in self.tender_codes:
                                hours['tender'] += h
                                week_hours[iday]['tender'] += h
                                overhead = True
                            if acode in self.training_codes:
                                hours['training'] += h
                                week_hours[iday]['training'] += h
                                overhead = True
                            if acode in self.holiday_codes:
                                hours['holiday'] += h
                                week_hours[iday]['holiday'] += h
                                overhead = True
                            if acode in self.sick_codes:
                                hours['sick'] += h
                                week_hours[iday]['sick'] += h
                                overhead = True
                            if acode in self.absent_codes:
                                hours['absent'] += h
                                week_hours[iday]['absent'] += h
                                overhead = True
                            if acode in self.offwork_codes:
                                hours['offwork'] += h
                                week_hours[iday]['offwork'] += h
                                overhead = True
                            #
                            # project specific
                            if not overhead:
                                #try:
                                #    print(acode)
                                #    codeparts = acode.split('.')
                                #except AttributeError:
                                #    codeparts = ["%d" %acode]
                                #    print("Note, could not split code %s\n" %(acode))
                                codeparts=str(acode)
                                if codeparts.find("No Code Yet")==0:
                                    desc = worksheet.cell_value(curr_row, ws_style['desc'])
                                    donereplace = False
                                    for k,v in self.missing.iteritems():
                                        if desc.upper().find(k.upper())>=0:
                                            codeparts=v
                                            donereplace = True
                                    if not donereplace:
                                        print("Unknown Project ID %s" %desc)
                                    
                                # is this person supposed to puting time against this code?
                                permitted = True
                                if template.has_key(codeparts):
                                    if len(template[codeparts])>0:
                                        permitted = False
                                        if STAFF[self.person].has_key("roles"):
                                            for role in STAFF[self.person]["roles"]:
                                                if role in template[codeparts]:
                                                    permitted = True
                                                    break
                                if not permitted:
                                    cname = codeparts
                                    if codenames.has_key(codeparts):
                                        cname = codeparts + " ("+codenames[codeparts]+")"
                                    if STAFF[self.person].has_key("roles"):
                                        Problems.append(Problem(self.person, wsname, ProblemKeys.NOT_PERMITTED, cname))
                                        #print("PROBLEM: worksheet on date %s has %s putting time against code %s" %(date,self.person, cname))
                                    else:
                                        Problems.append(Problem(self.person, wsname, ProblemKeys.NO_ROLES, cname))
                                                
                                if not hours.has_key(codeparts):
                                    hours[codeparts] = h
                                else:
                                    hours[codeparts] += h

                                if not week_hours[iday].has_key(codeparts):
                                    hours[codeparts] = h
                                    week_hours[iday][codeparts] = h
                                else:
                                    hours[codeparts] += h
                                    week_hours[iday][codeparts] += h
                        iday+=1

        if hours['total']> self.standard_hrs:
            Problems.append(Problem(self.person, wsname, ProblemKeys.OVER_ALLOCATED, hours['total']))
            if DEBUG:
                print("PROBLEM: worksheet on date %s has more than (%.2f)  hours (%.2f)" %(date, self.standard_hrs, hours['total']))

        if hours['total']<self.standard_hrs:
            Problems.append(Problem(self.person, wsname, ProblemKeys.UNDER_ALLOCATED, hours['total']))
            if DEBUG:
                print("PROBLEM: worksheet on date %s has less than (%.2f) hours (%.2f)" %(date, self.standard_hrs, hours['total']))

        self.days.extend(ws_dates)
        self.hours.extend(week_hours)
        self.days5.append(ws_date)
        self.hours5.append(hours)
        self.sheets.append(worksheet)


    def addSAP(self,worksheet):
        num_rows = worksheet.nrows - 1
        curr_row = -1
        required = 3
        start = 2
        num = 0

        while curr_row < num_rows:
            curr_row += 1
            if curr_row>=start:
                # Cell Types: 0=Empty, 1=Text, 2=Number, 3=Date, 4=Boolean, 5=Error, 6=Blank
                if worksheet.cell_type(curr_row, required)!=0: 
                    # only include lines that have the required fild not empty
                    who = worksheet.cell_value(curr_row,5)
                    cname = worksheet.cell_value(curr_row,1)
                    if who.lower().find(self.person.lower())==0  and cname != 'Changed after approval':
                        if self.cat_chargeable=='':
                            self.cat_chargeable=worksheet.cell_value(curr_row,2)
                        fdate = worksheet.cell_value(curr_row,6)
                        ws_date = datetime(*xlrd.xldate_as_tuple(fdate,0))
                        if not self.SAPhours.has_key(ws_date):
                            #print(ws_date)
                            self.SAPhours[ws_date] = {'total': 0.0, 'admin':0.0, 'it':0.0, 'tender':0.0, 'offwork':0.0, 'holiday':0.0, 'sick':0.0, 'absent':0.0, 'training':0.0}

                        h = worksheet.cell_value(curr_row,7)
                        try: 
                            self.SAPhours[ws_date]['total'] += h
                        except TypeError:
                            # it looks like SAP export had commas
                            newh = str(h).replace(',','.')
                            try:
                                self.SAPhours[ws_date]['total'] += float(newh)
                            except TypeError:
                                # time to give up
                                print("ERROR, could not convert \"%s\" to float" %h)
                            h = float(newh)
                            
                        #row = worksheet.row(curr_row)
                        #print row
                        #print(h)
                        num +=1
                        if worksheet.cell_type(curr_row, 10)!=0 and worksheet.cell_value(curr_row,10) :
                            wbs =  worksheet.cell_value(curr_row,10)
                            # we have a WBS element
                            #try:
                            #    codeparts = wbs.split('.')
                            #except AttributeError:
                            #    codeparts = ["%d" %wbs]
                            #    print("Note, could not split code %s\n" %(wbs))
                            codeparts = str(wbs)
                            #print(codeparts)
                            if not self.SAPhours[ws_date].has_key(codeparts):
                                self.SAPhours[ws_date][codeparts] = h
                            else:
                                self.SAPhours[ws_date][codeparts] += h
                        else:
                            # not a WBS element
                            acode = int(worksheet.cell_value(curr_row,8))
                            aname = worksheet.cell_value(curr_row,9)
                            if acode in self.admin_codes or aname in self.admin_codes:
                                self.SAPhours[ws_date]['admin'] += h
                            if acode in self.it_codes or aname in self.it_codes:
                                self.SAPhours[ws_date]['it'] += h
                            if acode in self.tender_codes or aname in self.tender_codes:
                                self.SAPhours[ws_date]['tender'] += h
                            if acode in self.training_codes or aname in self.training_codes:
                                self.SAPhours[ws_date]['training'] += h
                            if acode in self.holiday_codes or aname in self.holiday_codes:
                                self.SAPhours[ws_date]['holiday'] += h
                            if acode in self.sick_codes or aname in self.sick_codes:
                                self.SAPhours[ws_date]['sick'] += h
                            if acode in self.absent_codes or aname in self.absent_codes:
                                self.SAPhours[ws_date]['absent'] += h
                            if acode in self.offwork_codes or aname in self.offwork_codes:
                                self.SAPhours[ws_date]['offwork'] += h

        print("loaded %d SAP entries for %s" %(num,self.person))


def userCSV(userdata, decimalType='.'):
    nparts = userdata.person.split()
    fname = 'psy-ts-%s%s%s.csv' %(nparts[0][0].upper(),nparts[0][1].upper(),nparts[1][0].upper())
    fd = open(fname, 'wb')
    csvwriter = csv.writer(fd, delimiter=',',quotechar='"', quoting=csv.QUOTE_MINIMAL)
    
    days = userdata.getdates(fivedays=True)

    for d in days:
        userdata.getdatabydate(d.strftime("%Y%m%d"),csvwriter, decimalType=decimalType)

    fd.close()


def commonCSV(data, filename='psy-ts-output.csv',decimalType='.'):
    fd = open(filename, 'wb')
    csvwriter = csv.writer(fd, delimiter=',',quotechar='"', quoting=csv.QUOTE_MINIMAL)

    for userdata in sorted(data, key=lambda x: x.number):
        nparts = userdata.person.split()
        days = userdata.getdates(fivedays=True)

        for d in days:
            userdata.getdatabydate(d.strftime("%Y%m%d"),csvwriter, decimalType=decimalType)


    fd.close()

def projectCodes(data,template=None,limit=None):
    roles = []
    allcodes = []
    for userdata in sorted(data, key=lambda x: x.number):
        nparts = userdata.person.split()
        codes = userdata.getProjectsWS()
        for c in codes:
            if template:
                if template.has_key(c):
                    roles = template[c]
                else:
                    roles = []
            if c in allcodes:
                pass
            else:
                if limit:
                    if limit in roles:
                        allcodes.append(c)
                else:
                    allcodes.append(c)
        
    return(allcodes)

def nonprojectCodes(data):
    roles = []
    allcodes = []
    for userdata in sorted(data, key=lambda x: x.number):
        nparts = userdata.person.split()
        codes = userdata.getnonProjectsWS()
        for c in codes:
            if c in allcodes:
                pass
            else:
                allcodes.append(c)
        
    return(allcodes)

def codeHours(code,data,fivedays=False):
    totalhours = 0.0
    
    for userdata in sorted(data, key=lambda x: x.number):
        nparts = userdata.person.split()
        hours = sum(userdata.getdataWS(code,fivedays=fivedays))
        totalhours = totalhours+hours
        
    return(totalhours)


def listCodes(codes,cnames={},overhead=['it', 'tender', 'absent', 'training', 'admin', 'sick', 'offwork', 'holiday']):
    for code in codes:
        if cnames.has_key(code):
            print('%s (%s)' %(code,cnames[code]))
        else:
            if code in overhead:
                print('%s overhead' %(code))
            else:
                print('%s (unkown)' %(code))


def listCosts(codes,data,cnames={},overhead=['it', 'tender', 'absent', 'training', 'admin', 'sick', 'offwork', 'holiday']):
    totalhours = 0.0
    for code in codes:
        cost = codeHours(code,data)
        totalhours = totalhours + cost
        if cnames.has_key(code):
            print('%s (%s)\t %.2f' %(code,cnames[code],cost))
        else:
            if code in overhead:
                print('%s overhead\t %.2f' %(code,cost))
            else:
                print('%s (unkown)\t %.2f' %(code,cost))
    print('TOTAL %.2f' %totalhours)
    
                
def codeCosts(codes,data):
    totalhours = 0.0
    for code in codes:
        totalhours = totalhours + codeHours(code,data)

    return(totalhours) 


def userplot(userdata,fivedays=True):
    fig = plt.figure(1, figsize=(16,5),facecolor='#ffffff')
    ax = fig.add_subplot(111)

    d = userdata.getdates(fivedays=fivedays)

    (ut,isap) = userdata.getdata('total',fivedays=fivedays)
    (ua,xsap) = userdata.getdata('admin',fivedays=fivedays)
    (ui,xsap) = userdata.getdata('it',fivedays=fivedays)
    (us,xsap) = userdata.getdata('tender',fivedays=fivedays)
    (uo,xsap) = userdata.getdata('offwork',fivedays=fivedays)

    for i in range(0,len(d)):
        if isap[i]:
            ax.axvline(d[i],color='#aaaaaa',linestyle=':')

    p1 = ax.plot(d, ut)
    p2 = ax.plot(d, ua)
    p3 = ax.plot(d, ui)
    p4 = ax.plot(d, us)
    p5 = ax.plot(d, uo)

    plt.legend( (p1[0], p2[0], p3[0], p4[0], p5[0]), ('Total','Admin', 'IT', 'Tender', 'Off-Work'), loc=2,prop={'size':10} )    

    plt.title('%s [%s to %s]' %(userdata.person,d[0].strftime("%d/%m/%Y"),d[len(d)-1].strftime("%d/%m/%Y")), bbox=None)

    nparts = userdata.person.split()
    
    plt.savefig('psy-all-%s%s%s.png' %(nparts[0][0].upper(),nparts[0][1].upper(),nparts[1][0].upper()),bbox_inches='tight')    
    #plt.show()
    plt.close()

def userplotWS(userdata,fivedays=True):
    fig = plt.figure(1, figsize=(16,5),facecolor='#ffffff')
    ax = fig.add_subplot(111)

    d = userdata.getdatesWS(fivedays=fivedays)

    ut = userdata.getdataWS('total',fivedays=fivedays)
    ua = userdata.getdataWS('admin',fivedays=fivedays)
    ui = userdata.getdataWS('it',fivedays=fivedays)
    us = userdata.getdataWS('tender',fivedays=fivedays)
    uo = userdata.getdataWS('offwork',fivedays=fivedays)

    p1 = ax.plot(d, ut)
    p2 = ax.plot(d, ua)
    p3 = ax.plot(d, ui)
    p4 = ax.plot(d, us)
    p5 = ax.plot(d, uo)

    plt.legend( (p1[0], p2[0], p3[0], p4[0], p5[0]), ('Total','Admin', 'IT', 'Tender', 'Off-Work'), loc=2,prop={'size':10} )    

    plt.title('%s [%s to %s]' %(userdata.person,d[0].strftime("%d/%m/%Y"),d[len(d)-1].strftime("%d/%m/%Y")), bbox=None)

    nparts = userdata.person.split()
    
    plt.savefig('psy-%s%s%s.png' %(nparts[0][0].upper(),nparts[0][1].upper(),nparts[1][0].upper()),bbox_inches='tight')    
    plt.close()


def userplotSAP(userdata,fivedays=True):
    fig = plt.figure(1, figsize=(16,5),facecolor='#ffffff')
    ax = fig.add_subplot(111)

    d = userdata.getdatesSAP(fivedays=fivedays)

    ut = userdata.getdataSAP('total',fivedays=fivedays)
    ua = userdata.getdataSAP('admin',fivedays=fivedays)
    ui = userdata.getdataSAP('it',fivedays=fivedays)
    us = userdata.getdataSAP('tender',fivedays=fivedays)
    uo = userdata.getdataSAP('offwork',fivedays=fivedays)

    p1 = ax.plot(d, ut)
    p2 = ax.plot(d, ua)
    p3 = ax.plot(d, ui)
    p4 = ax.plot(d, us)
    p5 = ax.plot(d, uo)

    plt.legend( (p1[0], p2[0], p3[0], p4[0], p5[0]), ('Total','Admin', 'IT', 'Tender', 'Off-Work'), loc=2,prop={'size':10} )    

    plt.title('SAP %s [%s to %s]' %(userdata.person,d[0].strftime("%d/%m/%Y"),d[len(d)-1].strftime("%d/%m/%Y")), bbox=None)

    nparts = userdata.person.split()
    
    plt.savefig('psy-sap-%s%s%s.png' %(nparts[0][0].upper(),nparts[0][1].upper(),nparts[1][0].upper()),bbox_inches='tight')    
    plt.close()


def projectplot(code,project,userdata,fivedays=True):
    fig = plt.figure(1, figsize=(16,5),facecolor='#ffffff')
    ax = fig.add_subplot(111)

    projectdates = {}

    for ud in userdata:
        dts = ud.getdates(fivedays=fivedays)
        (phr,isap) = ud.getdata(code,fivedays=fivedays)

        for i in range(0,len(phr)):
            if phr[i]>0.0:
                if projectdates.has_key(dts[i]):
                    projectdates[dts[i]] = projectdates[dts[i]] + phr[i]
                else:
                    projectdates[dts[i]] = phr[i]

    dates = []
    hours = []
    for d,h in sorted(projectdates.iteritems(), key=operator.itemgetter(0)):
        dates.append(d)
        hours.append(h)

    p1 = ax.plot(dates, hours)

    plt.title('%s [%s to %s]' %(project,dates[0].strftime("%d/%m/%Y"),dates[len(dates)-1].strftime("%d/%m/%Y")), bbox=None)

    plt.savefig('psy-%s.png' %(project.upper()),bbox_inches='tight')    
    plt.close()

def actuals(code,project,userdata,duration=24):
    projectdates = {}

    for ud in userdata:
        dts = ud.getdates(fivedays=False)
        (phr,isap) = ud.getdata(code,fivedays=False)

        for i in range(0,len(phr)):
            if phr[i]>0.0:
                if projectdates.has_key(dts[i]):
                    projectdates[dts[i]] = projectdates[dts[i]] + phr[i]
                else:
                    projectdates[dts[i]] = phr[i]

    dates = []
    hours = []
    for d,h in sorted(projectdates.iteritems(), key=operator.itemgetter(0)):
        dates.append(d)
        hours.append(h)

    # now calculate monthly totals
    manhours = [0.0]*duration 
    month = 7

    results = {}
    for i in range(0,len(dates)):
        if (results.has_key(str(dates[i].month) + "-" + str(dates[i].year))):
           results[str(dates[i].month) + "-" + str(dates[i].year)] += hours[i]
        else :
           results[str(dates[i].month) + "-" + str(dates[i].year)] = hours[i]


    overallTotal = 0
    print ("Actuals for %s Code" %(code))
    print ("===================")
    for (k,v) in sorted(results.iteritems(), key=operator.itemgetter(0)):
        print ("Month %s Total = %s  ManDays (%s hours)" %(k,v/7.5, v ))
        overallTotal += v       
    print("Overall Total = %s ManDays (%s hours)" %(overallTotal/7.5, overallTotal))
##        if dates[i].month == month:
##            manhours[month] += hours[i]
##        else:
##            if dates[i].month == month+1:
##                month +=1
##                manhours[month] += hours[i]
##
##    mandays = manhours
##    for i in range(0,len(mandays)):
##        mandays[i] = manhours[i]/7.5
##    print manhours

def iso_year_start(iso_year):
    "The gregorian calendar date of the first day of the given ISO year"
    fourth_jan = datetime.strptime('{0}-01-04'.format(iso_year), '%Y-%m-%d')
    delta = timedelta(fourth_jan.isoweekday()-1)
    return fourth_jan - delta 

def iso_to_gregorian(iso_year, iso_week, iso_day):
    "Gregorian calendar date for the given ISO year, week and day"
    year_start = iso_year_start(iso_year)
    return year_start + timedelta(days=iso_day-1, weeks=iso_week-1)


def filenameParse(filename,person,directory=True,year=2014):
    rdate = None
    (folder,fname) = os.path.split(filename)
    
    if fname.find("~")==0:
        return None

    if fname.find(".")==0:
        return None

    if directory:
        s = re.search('[a-zA-Z]+_Timesheet_(\d+)\.xlsx',fname)
        if s:
            res = s.groups()
            rdate = None
            try:
                rdate = datetime.strptime(res[0], '%Y%m%d')
            except:
                print("could not parse filename \"%s\"" %fname)
            return rdate

        s = re.search('Timesheet_(\d+)_(\d+)_[a-zA-Z_]+\.xlsx',fname)
        if s:
            res = s.groups()
            rdate = None
            try:
                rdate = iso_to_gregorian(int(res[1]),int(res[0]),1)
            except:
                print("could not parse filename \"%s\"" %fname)
            return rdate

        s = re.search('Timesheet_W(\d+)_[a-zA-Z_]+_(\d+)\.xlsx',fname)
        if s:
            res = s.groups()
            rdate = None
            try:
                rdate = datetime.strptime(res[1], '%Y%m%d')
            except:
                print("could not parse filename \"%s\"" %fname)
            return rdate



    # person specific filenames
    lp = person.lower().replace(' ','_')
    lf = filename.lower()
    ls = 'timesheet_week(\d+)_%s\.xlsx' %lp

    s = re.search(ls,lf)
    if s:
        res = s.groups()
        rdate = None
        try:
            rdate = iso_to_gregorian(year,int(res[0]),1)
        except:
            print("could not parse filename \"%s\"" %fname)
        return rdate    

    lp = person.lower().replace(' ','_')
    lf = filename.lower()
    ls = 'timesheet_(\d+)_%s\.xlsx' %lp

    s = re.search(ls,lf)
    if s:
        res = s.groups()
        rdate = None
        try:
            rdate = iso_to_gregorian(year,int(res[0]),1)
        except:
            print("could not parse filename \"%s\"" %fname)
        return rdate    

    lp = person.lower().replace(' ','_')
    lf = filename.lower()
    ls = 'timesheet_(\d+)_(\d+)_%s\.xlsx' %lp

    s = re.search(ls,lf)
    if s:
        res = s.groups()
        rdate = None
        try:
            rdate = iso_to_gregorian(int(res[1]),int(res[0]),1)
        except:
            print("could not parse filename \"%s\"" %fname)
        return rdate

    lp = person.lower().replace(' ','_')
    lf = filename.lower()
    ls = 'timesheet_W(\d+)_(\d+)_%s\.xlsx' %lp

    s = re.search(ls,lf)
    if s:
        res = s.groups()
        rdate = None
        try:
            rdate = iso_to_gregorian(int(res[1]),int(res[0]),1)
        except:
            print("could not parse filename \"%s\"" %fname)
        return rdate        

    return rdate

def findSheets(person,base_path="/mnt/timetracking/Operations",all=False):
    res = {}
    dirList = os.listdir(base_path)
    for fname in dirList:
        # Named directories
        if fname==person and os.path.isdir(base_path + os.sep + fname):
            persondir = base_path + os.sep + fname
            personList = os.listdir(persondir)
            for xname in personList:
                xdate = filenameParse(xname,person,directory=True)
                if xdate:
                    res[xdate.strftime('%Y%m%d')] = xname
            break
        # Named files
        if os.path.isfile(base_path + os.sep + fname):
           
            xdate = filenameParse(fname,person,directory=False)
            if xdate:
                fullpath = base_path + os.sep + fname
                #print fullpath
                res[xdate.strftime('%Y%m%d')] = fullpath

    return res

def processSheets(person,info,sheets,version=1,allSetValues={}, base_path="", units=[],sap_path="C:\Apps\Python27"):
    res = TimeSheet(person,info)
    cnt = 0
    template = {}
    codenames = {}
    #print sheets
    for (wsdate,wsfile) in sorted(sheets.iteritems(), key=operator.itemgetter(1)):
        if base_path == "":
            wsname = wsfile
        else:
            wsname = base_path+os.sep+wsfile

        if (wsdate not in allSetValues):
            allSetValues[wsdate] = {}

        workbook = None
        allSetValues[wsdate][person] = info['group']
        try:
            workbook = xlrd.open_workbook(wsname)
        except (IOError):
            print ("PROBLEM: Loading Worksheet %s" %(wsname))
        except:
            print ("ERROR: Loading Worksheet %s" %(wsname))            

        if (workbook != None):
            
            # check the sheet names
            wsnames = workbook.sheet_names()
                
            # search for "old" sheets
            modern = True
            for sheet in wsnames:
                if sheet==u'URTDSM':
                    modern=False

            if modern:
                worksheet = workbook.sheet_by_name(u'Timesheet')
                templatesheet = workbook.sheet_by_name(u'List data')

                wstype = 'original'
                if worksheet.cell_type(0, 0)!=0:
                    if worksheet.cell_value(0, 0) == u'Week':
                        wstype = 'new1'
                if worksheet.cell_value(0,1) == u'Week':
                        wstype = 'new2'
            else:
                worksheet = workbook.sheet_by_name(u'URTDSM')
                templatesheet = None
                wstype = 'old'

            if DEBUG:
                print("Loading timesheet %s for %s type %s" %(wsfile,person,wstype))
            template,codenames = loadTemplate(templatesheet,template,codenames)
            
            if len(units)==len(sheets):
                res.addWorkSheet(wsdate,wsname,worksheet,wstype, version, template, codenames, units=units[cnt])
            else:
                res.addWorkSheet(wsdate,wsname,worksheet,wstype, version, template, codenames)


        
        cnt+=1

    for sap in SAP:
        wsname = sap_path+os.sep+sap
        try:
            workbook = xlrd.open_workbook(wsname)
        except Exception as e:
            print e
            print("Could not find SAP file %s" %(wsname))
            continue

        worksheet = workbook.sheet_by_index(0)
        res.addSAP(worksheet)

    res.codenames = codenames
    res.template = template
        
    return res

def processTemplateFile(filename='/mnt/timetracking/Timesheet_weekNumber_Year_FirstName_SurnameV25a.xlsx'):
    templateFile=filename
    template = {}
    codenames = {}


    try:
        templatebook = xlrd.open_workbook(templateFile)
    except (IOError):
        print ("PROBLEM: Loading Worksheet %s" %(templateFile))
    except:
        print ("ERROR: Loading Worksheet %s" %(templateFile))

    if (templatebook != None):
        worksheet = templatebook.sheet_by_name(u'List data')
        template,codenames = loadTemplate(worksheet,{},{})
                            
    return (template,codenames)

def loadTemplate(worksheet,template,codenames):
    startrow=2
    required=1
    
    num_rows = worksheet.nrows - 1
    curr_row = -1
    while curr_row < num_rows:
        curr_row += 1
        if curr_row>=startrow:
            # Cell Types: 0=Empty, 1=Text, 2=Number, 3=Date, 4=Boolean, 5=Error, 6=Blank
            if worksheet.cell_type(curr_row, required)!=0: 
                # only include lines that have the required fild not empty
                code = worksheet.cell_value(curr_row, 1)
                codename = (worksheet.cell_value(curr_row, 0)).encode('utf-8')
                if codename.find('3415')==0:
                    sp = codename.find(' ')
                    codename = codename[(sp+1):]
                    if codename.find('- ')==0:
                        codename = codename[2:]
                if code.find('3415')==0:
                    try:
                        role = worksheet.cell_value(curr_row, 2)
                    except IndexError:
                        pass
                    else:
                        if len(role)>0:
                            template[str(code)] = [str(r).strip().upper() for r in role.split(',')]
                        else:
                            template[str(code)] = []
                    codenames[str(code)] = codename
    return (template,codenames)


def missingSheets(setSheets):
    remaining = {}
    for date, valueRemaining in setSheets.iteritems():
        for (key, value) in  STAFF.iteritems():
            if key not in valueRemaining and (not value.has_key('startdate') or datetime.strptime(date, '%Y%m%d') >= datetime.strptime(value['startdate'], '%d/%m/%Y')):
                if date not in remaining:
                    remaining[date] = {}
                remaining[date][key] = value['group']
    return remaining

def findInApproved(group, GROUPS, approvedPeople):
    try:
        dirList = os.listdir(GROUPS[group]["approved"])
        for fname in dirList:
            # Named directories
            if os.path.isdir(GROUPS[group]["approved"] + os.sep + fname):
                persondir = GROUPS[group]["approved"] + os.sep + fname
                personList = os.listdir(persondir)
                if personList is not None:
                    for xname in personList:
                        if xname is not None:
                            (xdate, person) = findTimesheetNameAndDate(xname)
                            if xdate is not None and xdate not in approvedPeople:
                                approvedPeople[xdate] = []
                            if person is not None:
                                approvedPeople[xdate].append(person)
    except Exception as e:
        print e
    return approvedPeople

def findTimesheetNameAndDate(filename):
    lf = filename.lower()
    ls = 'timesheet_(\d+)_(\d+)_(\w+)\.xlsx'
    s = re.search(ls,lf)

    pattern = re.compile(ls)
    if pattern.match(lf):
        if s is not None and s:
            res = s.groups()
            rdate = None        
            try:
                rdate = iso_to_gregorian(int(res[1]),int(res[0]),1)
            except:
                print("could not parse filename \"%s\"" %fname)
            return (rdate.strftime("%Y%m%d"), res[2].replace('_',' '))
    return (None,None)

def filterApprovedRemaining(remaining, approvedPeople):
    remainingFiltered = {}

    for date in remaining:
        for item in remaining[date].iteritems():
            if not date in approvedPeople.keys() or not item[0].lower() in approvedPeople[date]:
                if date is not None and date not in remainingFiltered: 
                    remainingFiltered[date] = {}
                remainingFiltered[date][item[0]] = item[1]             
    return remainingFiltered

def buildApprovedList(GROUPS):
    approvedPeople = {}
    approvedPeople = findInApproved("ops", GROUPS, approvedPeople)
    approvedPeople = findInApproved("dev", GROUPS, approvedPeople)
    approvedPeople = findInApproved("pst", GROUPS, approvedPeople)
    return approvedPeople
                

def printOutput(problems, remainingFiltered, GROUPS):

    #All Staff missing is assumed as no staff missing. May seem strange but is done because individual groups may be marked
    #as missing if anything in any of the other groups exists.
    opsStaff = filter(lambda x: x['group'] == 'ops' or x['group'] == 'pms', STAFF.values())
    devStaff = filter(lambda x: x['group'] == 'dev', STAFF.values())
    pstStaff = filter(lambda x: x['group'] == 'ops', STAFF.values())

    if not IGNORE_INVALID_TIMESHEET_VERSION:
        print "************************************"    
        print "* Invalid Timesheets Version List: *"
        print "************************************"    
        for problem in [p for p in problems if p.key == ProblemKeys.INVALID_VERSION]:
            print ("%s for %s [%s] actual value [%s]" %(problem.key, problem.person, problem.sheet, problem.value))
        print "**********************************"

    print "**********************************"    
    print "* Missing Ops / PMS Staff      : *"
    print "**********************************"
    for date in sorted(remainingFiltered.keys(), key=lambda x: datetime.strptime(x, "%Y%m%d") ):
        opsItems = [k for k, v in remainingFiltered[date].iteritems() if v == "ops" or v == "pms"]
        if (len(opsItems) > 0):
            print "_________________"
            print ("%s [%d]" %(datetime.strptime(date, "%Y%m%d").strftime("%d/%m/%Y"),datetime.strptime(date, "%Y%m%d").isocalendar()[1]))
            print ""
            if (len(opsItems) <> len(opsStaff)):
                for person in opsItems:
                    print person
    print ""
    print ""
                
    
    print "**********************************"    
    print "* Missing Dev Staff            : *"
    print "**********************************"    
    for date in sorted(remainingFiltered.keys(), key=lambda x: datetime.strptime(x, "%Y%m%d") ):
        devItems = [k for k, v in remainingFiltered[date].iteritems() if v == "dev"]
        if (len(devItems) > 0):
            print "_________________"
            print ("%s [%d]" %(datetime.strptime(date, "%Y%m%d").strftime("%d/%m/%Y"),datetime.strptime(date, "%Y%m%d").isocalendar()[1]))
            print ""
            if (len(devItems) <> len(devStaff)):        
                for person in devItems:
                    print person
    print ""
    print ""

    print "**********************************"    
    print "* Missing PST Staff            : *"
    print "**********************************"    
    for date in sorted(remainingFiltered.keys(), key=lambda x: datetime.strptime(x, "%Y%m%d") ):
        pstItems = [k for k, v in remainingFiltered[date].iteritems() if v == "pst"]
        if (len(pstItems) > 0):
            print "_________________"
            print ("%s [%d]" %(datetime.strptime(date, "%Y%m%d").strftime("%d/%m/%Y"),datetime.strptime(date, "%Y%m%d").isocalendar()[1]))
            print ""
            if (len(pstItems) <> len(pstStaff)):        
                for person in pstItems:
                    print person
    print ""
    print ""

    print "************************************"    
    print "* Invalid Timesheets Hours List:   *"
    print "************************************"    
    for problem in [p for p in problems if p.key == ProblemKeys.OVER_ALLOCATED or p.key == ProblemKeys.UNDER_ALLOCATED]:
        print ("%s for %s [%s] actual value [%s]" %(problem.key, problem.person, problem.sheet, problem.value))
    print "**********************************"


    print "************************************"    
    print "* Codes Not Permitted:             *"
    print "************************************"    
    for problem in [p for p in problems if p.key == ProblemKeys.NOT_PERMITTED or p.key == ProblemKeys.NO_ROLES]:
        if problem.key==ProblemKeys.NOT_PERMITTED:
            print ("%s [%s] is not permitted to use code %s" %(problem.person, problem.sheet, problem.value))
        else:
            print ("%s [%s] has no roles defined, so not sure if they are permitted to use role-restricted code %s" %(problem.person, problem.sheet, problem.value))
    print "***********************************"    
       
def main():

    processed = []

    version = 28 #raw_input("Please enter expected Timesheet version: ")

    allOpsStaff = {}
    allDates = {}
        
    if sys.platform.startswith('win'):
        GROUPS = WINDOWS_GROUPS
    else:
        GROUPS = LINUX_GROUPS
    
    for person, info in STAFF.iteritems():
        if info['group']=="ops" or info['group']=="pms":
            if (os.path.exists(GROUPS[info['group']]['path'])):
                s = findSheets(person,base_path=GROUPS[info['group']]['path'])
                p = processSheets(person,info,s, version, allDates)
                processed.append(p)

    commonCSV(processed,filename="psy-ts-OPS.csv")
    
    dev_processed = []
    for person, info in STAFF.iteritems():
        if info['group']=="dev":
            if (os.path.exists(GROUPS[info['group']]['path'])):
                s = findSheets(person,base_path=GROUPS[info['group']]['path'])
                p = processSheets(person,info,s, version, allDates )
                dev_processed.append(p)

    commonCSV(dev_processed,filename="psy-ts-Dev.csv")


    
    pst_processed = []
    for person, info in STAFF.iteritems():
        if info['group']=="pst":
            if (os.path.exists(GROUPS[info['group']]['path'])):
                s = findSheets(person,base_path=GROUPS[info['group']]['path'])
                p = processSheets(person,info,s, version, allDates)
                pst_processed.append(p)

    commonCSV(pst_processed,filename="psy-ts-PST.csv")
 
    approvedList = buildApprovedList(GROUPS)
    missingStaffSheets = missingSheets(allDates)
    missingStaffFiltered = filterApprovedRemaining(missingStaffSheets, approvedList)
    printOutput(Problems, missingStaffFiltered, GROUPS)

    allprocessed = processed + dev_processed + pst_processed
 ##   projectplot(u'3415P5046-06.3.3.3','VISOR IM',allprocessed)
 ##   actuals(u'3415P5046-06.3.3.3','VISOR IM',allprocessed)    

if __name__ == '__main__':
    main()
