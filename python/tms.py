import requests
import json
import pytz
import dateutil.parser #pip install python-dateutil
from dateutil.relativedelta import relativedelta, MO
from datetime import datetime,timedelta, timezone, tzinfo

  
response = requests.post('http://tms.routegiant.com/auth/local', json={"email":"lachlan.macpherson@routemonkey.com","password":"h574vp"})

json_data = json.loads(response.text)
auth_header = "bearer " + json_data["token"]

payload = {'conditions': '{"archived":{"$ne":true}}', 'sort': 'endDate'}
headers = {'Authorization' : auth_header}
response = requests.get('http://tms.routegiant.com/api/tasks', params=payload, headers=headers)

today = datetime.now()
## If today is not day 5 or 6 e.g. Saturday or Sunday
if (today.weekday() < 4):
    last_monday = today + relativedelta(weekday=MO(-2))
    next_monday = today + relativedelta(weekday=MO(-1))
else:
    last_monday = today + relativedelta(weekday=MO(-1))
    next_monday = today + relativedelta(weekday=MO(1))    
last_monday = last_monday.replace(hour=0, minute=0, second=0, microsecond=0).strftime('%Y-%m-%d %H:%M:%S')
next_monday = next_monday.replace(hour=0, minute=0, second=0, microsecond=0)

json_data = json.loads(response.text)
count = 0
for record in json_data:
    str_val = record['weekCommenceDate']
    if str_val is not None:
        date_record = dateutil.parser.parse(str_val).strftime('%Y-%m-%d %H:%M:%S')
        if (date_record == last_monday):
            print("Record {0} to be updated to {1}:...".format(record['jobNumber'], next_monday.strftime('%d-%m-%Y')), end='')
            record['weekCommenceDate'] = next_monday.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z'
            response = requests.put('http://tms.routegiant.com/api/tasks/' + record['_id'], headers=headers, json=record)
            print("Done.", flush=True)
            count+=1
print("Total {} Records updated".format(count))


