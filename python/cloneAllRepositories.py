import requests
import json
import pytz
from subprocess import call

import Cipher
import sys

USER_DETAILS = {
    "Phil" : {"repoName" : "cabalamat",
              "repoType" : "git",
              "repoSuffix" : ".git",    
              "dirName" : "Phil"},
    "Stacy" : {"repoName" : "StacyRendall",
              "repoType" : "git",
              "dirName" : "Stacy",
              "repoSuffix" : ".git"},    
    "Thomas" : {"repoName" : "ThomasFord",
              "repoType" : "hg",
              "repoSuffix" : "",    
              "dirName" : "Thomas"},
    "Osama" : {"repoName" : "OKhalifa",
              "repoType" : "hg",
              "repoSuffix" : "",                   
              "dirName" : "Osama"}    
}
class BitBucketClone():

    def __init__(self, crypt_key):
        self.cipher = Cipher.AESCipher(crypt_key)
        
    def getRepositories(self, name):
        response = requests.get('https://api.bitbucket.org/1.0/users/{}'.format(name),auth=('lachlan.macpherson@routemonkey.com', self.cipher.decrypt('TgiQ6dO6cF+Kc5wbeWMCvcod0t2sKxrCH0uwX8ZfszQ=').decode("utf-8")))
        json_data = json.loads(response.text)
        repositories = []
        for record in json_data["repositories"]:
            if record["slug"] is not None:
                repositories.append(record["slug"])
        return repositories
            
def main():
    cloning = BitBucketClone(sys.argv[1])
    for (key,value) in USER_DETAILS.items():
        print(value)
        for repo in cloning.getRepositories(value["repoName"]):
            repoFullPath = "https://LachlanMacpherson@bitbucket.org/{}/{}{}".format(value["repoName"], repo, value["repoSuffix"])
            dirFullPath = "C:\\Source\\BitBucketExport\\{}\\{}".format(value["dirName"], repo)
            print ("Cloning {}'s repository {} to {}".format(key, repoFullPath, dirFullPath))
            call([value["repoType"], "clone", repoFullPath , dirFullPath])

if __name__ == '__main__':
    main()

    
