# Remember, you need "set SVN_SSH="C:\\apps\\PuTTY\\plink.exe" in windows for SVN to work...

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import re
import calendar
import random
import simplejson as json
from subprocess import Popen, PIPE
from restkit import Resource, BasicAuth, request, Connection
from socketpool import ConnectionPool
from datetime import datetime
from natsort import natsorted
from collections import OrderedDict

class DateClass:
    def __init__(self, year, month):
       self.month = month
       self.year = year
    def __hash__(self):
        return hash(self.month) + hash(self.year)
    def __eq__(self, rhs):       
        return isinstance(rhs, DateClass) and self.month == rhs.month and self.year == rhs.year

class Year:
   def __init__(self, year):
      self.year = year
      self.count = 0
      self.months = {}
      
   def __getitem__(self, name):      
       return self.count    
       
   def __hash__(self):
       return hash(self.year) + hash(self.count)
   def __eq__(self, rhs):       
       return isinstance(rhs, Year) and self.year == rhs.year and self.count == rhs.count
    
             
class Person:
    def __init__(self, name):
       self.name = name
       self.years = {}
       
    def inc_monthyear(self, yearNumber, monthNumber ):
        
        if not (self.years.has_key(yearNumber)):
           self.years[yearNumber] = Year(yearNumber)

        if not (self.years[yearNumber].months.has_key(monthNumber)):
           self.years[yearNumber].months[monthNumber] = 0
        self.years[yearNumber].months[monthNumber] +=1
        self.years[yearNumber].count += 1

        
class SvnSearch:

    def __init__(self, path):
       self.path = path

    def get_latest_revision(self):
        p = Popen('svn info %s' %(self.path),
          stdout=PIPE)
        stdout, stderr = p.communicate()
        regularExpression = re.compile('(Revision: )(\d+)')
        last_revision_kvp = regularExpression.findall(stdout)[0]
        return int(last_revision_kvp[1])
          
    def find_issues(self, start_revision, end_revision):
        #seems that in svn logs, : is being accidentally used rather than - for nmsproject key references. Convert : to - so nmsproject understands        
        p = Popen('svn log -r %d:%d %s' %(startrevision, endrevision, self.path),
          stdout=PIPE)
        stdout, stderr = p.communicate()
        regularExpression = re.compile('(r\d+ \| [a-z]* \| [0-9\-]*)')
        keys = regularExpression.findall(stdout)
        return list(set(keys))

class MyPlot:
   def __init__(self, data, plot, minYear, maxYear, branch):
       self.data = data
       self.plot = plot
       self.minYear = minYear
       self.maxYear = maxYear
       self.branch = branch
      
   def draw(self):
      fig, ax =  self.plot.subplots(figsize=(20,10))

      
      colors=["#3d7f42","#d158bb","#5ab84b","#7168dc","#a7b338","#914eb0","#cea339","#6375c0","#df8830","#5fa1d8","#d65230","#3dbbb8","#c93a50","#5bba88","#dc4e8b","#9cb369","#c88bcd","#6a7529","#9b496e","#8c6d2c","#dd7c80","#d99f6b","#a65831"]
      N = len(OrderedDict(sorted(self.data.items(), key=lambda t: t[0])).keys())
      ind = np.arange(N)
      width = 0.5 

              
#      oldmap = sorted(personClass.years.values(), key=lambda t: t[0])
#      newmap = map(lambda x: x.count, oldmap)
 
      #pad missing years.
      for personClass in self.data.values():
          for x in range(int(self.minYear), int(self.maxYear)+1):
              if not personClass.years.has_key(str(x)):
                  personClass.years[str(x)] = Year(str(x))
      bars = []
      firstcolourIndex = -1
      bottomSize = []
      allArrays = []
      index = 0
      for x in range(int(self.minYear), int(self.maxYear)+1):            
          year_arr = []
          for person, personClass in OrderedDict(sorted(self.data.items(), key=lambda t: t[0])).items():
              year_arr.append(int(personClass.years[str(x)].count))
          if firstcolourIndex == -1:
              firstcolourIndex = random.randrange(0, 22)

          allArrays.append(year_arr)
          if index == 0:
             b = ax.bar(ind, year_arr , width, color=colors[firstcolourIndex], label='Label')
          else:
             testIndex = 0
             bottomStart = np.zeros(len(year_arr))
             while testIndex < index:
                 bottomStart = [x + y for x, y in zip(bottomStart, allArrays[testIndex])]
                 testIndex += 1
             
             b = ax.bar(ind, year_arr , width, color=colors[firstcolourIndex], bottom=bottomStart, label='Label')              
          bars.append(b)
            
          #spread colours 1 apart.
          firstcolourIndex += 1
          if firstcolourIndex > 22:
              firstcolourIndex = 0
          index += 1

      for j in xrange(len(bars)):
          for i, patch in enumerate(bars[j].get_children()):
              bl = patch.get_xy()
              x = 0.5*patch.get_width() + bl[0]
              y = 0.5*patch.get_height() + bl[1]
              if allArrays[j][i] > 10:
                 ax.text(x,y, "%d" % (allArrays[j][i]), ha='center')
            
      ax.set_ylabel('Total')
      ax.set_xlabel('Person')
      ax.set_title('People by Year Totals (SVN Checkins for %s)' % (self.branch))
      self.plot.xticks(ind + (width/2), OrderedDict(sorted(self.data.items(), key=lambda t: t[0])).keys())
      self.plot.legend(loc='upper right')
      self.plot.legend(bars, range(int(self.minYear), int(self.maxYear)+1))
      self.plot.setp(labels, rotation=90)
      self.plot.savefig('Checkins per Year (%s-%s) %s.png' % (self.minYear, self.maxYear, self.branch.replace('/','_')),bbox_inches='tight')    
      self.plot.close()
  
def build_personDictionary(keys):
    personDictionary = {}
    minYear = np.inf
    maxYear = 0
    for key in keys:
        regularExpression = re.compile('(r\d+) \| ([a-z]+) \| ([0-9]{4})\-([0-9]+)')
        m = regularExpression.match(key)
 
        if not personDictionary.has_key(m.group(2)):
           personDictionary[m.group(2)] = Person(m.group(2))
           
           if  int(m.group(3)) < minYear:
               minYear = int(m.group(3))
           if int(m.group(3)) > maxYear:
               maxYear = int(m.group(3))
        personDictionary[m.group(2)].inc_monthyear(m.group(3),(m.group(4)))
    return (personDictionary, minYear, maxYear)

base_path = "svn+ssh://lachlan@cypress.edi.psymetrix.com/data/repositories/pmx-msf/"
#path = "phasorpoint"
#startrevision =  30019 # 2016 onwards
#startrevision =  22302 # 2015 onwards
#startrevision =  16744 # 2014 onwards
startrevision =  13187 # 2013 onwards
#startrevision=17093 #start of urtdsm logs
path = "phasorpoint/trunk"
#startrevision =  26278 # 2013 onwards

project_name = path.rsplit('/', 1)[-1]
endrevision = "HEAD"

svn_search = SvnSearch(base_path+path)
if (endrevision == 'HEAD'):
   endrevision = svn_search.get_latest_revision()
keys = svn_search.find_issues(startrevision, endrevision)

returnedValue = build_personDictionary(keys)

plt = MyPlot(returnedValue[0], plt, returnedValue[1], returnedValue[2], path)
plt.draw()

for person, personClass in OrderedDict(sorted(returnedValue[0].items(), key=lambda t: t[0])).items():
   print('\n_______________________\n%s' %(person))

   orderedYears = OrderedDict(sorted(personClass.years.items(), key=lambda t: t[0]))
   for year, yearClass in  orderedYears.items():
      print('Total %s=%s' %( year, yearClass.count))
      for month, countMonth in OrderedDict(sorted(yearClass.months.items(), key=lambda t: t[0])).items():
          print('% 10s-%s=%s' %(calendar.month_abbr[int(month)], year, countMonth)) 

