import requests
import json
import pytz
import subprocess
import shlex
from subprocess import call
from requests.auth import HTTPBasicAuth
import os
SOURCE_PATH = "C:\\Source\\BitBucketExport\\"
GIT_PATH = "ssh://git@bitbucket.trakm8.local:7999/rout/{}.git"

USER_DETAILS = {
    "Phil" : {"repoName" : "cabalamat",
              "dirName" : "Phil"},
    "Stacy" : {"repoName" : "StacyRendall",
              "dirName" : "Stacy"},
    "Thomas" : {"repoName" : "ThomasFord",
              "dirName" : "Thomas"}
}
class TM8UploadRepos():

    def createRepos(self, name):
        jsonPayload = {"name" : name, "has_wiki": True, "is_private": False, "project": {"key": name}}
        print (jsonPayload)
        #verify=False to ignore certificate errors
        response = requests.post('https://bitbucket.trakm8.local/rest/api/1.0/projects/ROUT/repos', auth=HTTPBasicAuth('lachlan.macpherson', 'Lexus234!!'), json=jsonPayload, verify=False)
        print (response)
        assert(response.status_code == 201)
        
    def getRepositories(self, name):
        response = requests.get('https://api.bitbucket.org/1.0/users/{}'.format(name),auth=('lachlan.macpherson@routemonkey.com', 'Lexus456'))
        json_data = json.loads(response.text)
        repositories = []
        for record in json_data["repositories"]:
            if record["slug"] is not None:
                repositories.append(record["slug"])
        return repositories
            
def main():
    
    upload = TM8UploadRepos()
    for dir_name in os.listdir(SOURCE_PATH):
        fullPath = os.path.join(SOURCE_PATH, dir_name)
        if os.path.isdir(fullPath):
            for file_name in os.listdir(fullPath):
               fullPath_inner = os.path.join(SOURCE_PATH , dir_name, file_name)
               if os.path.isdir(fullPath_inner):
                   print("Creating repository {}".format(file_name))
                   upload.createRepos(file_name)
                   print("Uploading {}'s repo {} to {}".format(dir_name,file_name, GIT_PATH.format(file_name)))
                   for_string = "for /f %r in ('git -C C:\\Source\\BitBucketExport\\{}\\{} branch -r ^| grep -v master') do git -C C:\\Source\\BitBucketExport\\{}\\{} checkout --track %r".format(dir_name, file_name, dir_name, file_name)
                   call(["cmd", "/c", for_string])
                   call(["git", "-C", fullPath_inner , "remote", "set-url", "origin", GIT_PATH.format(file_name)])
                   call(["git", "-C", fullPath_inner , "push", "--all"])
    #upload.createRepos("aerialex")
    
if __name__ == '__main__':
    main()

    
