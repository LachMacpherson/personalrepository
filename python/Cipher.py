from Crypto.Cipher import AES #pip install PyCrypto
##Python 3.5 script
import base64
import sys
import hashlib
#Need to change C:\Users\lachlan.macpherson\AppData\Local\Programs\Python\Python35\Lib\site-packages\Crypto\Random\OSRNG to show "from . import winrandom"
from Crypto import Random #pip install PyCrypto

BS = 16
pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)
unpad = lambda s :  s[0:-s[-1]]

#'1234567890123456'
#msg_text = 'h574vp'.rjust(32)
#msg_text = 'Lexus456'.rjust(32)
class AESCipher:
    def __init__( self, key ):
        self.key = hashlib.sha256(key.encode('utf-8')).digest()

    def encrypt( self, raw ):
        raw = pad(raw)
        iv = Random.new().read( AES.block_size )
        cipher = AES.new( self.key, AES.MODE_CBC, iv )
        return base64.b64encode( iv + cipher.encrypt( raw ) ) 

    def decrypt( self, enc ):
        enc = base64.b64decode(enc)
        iv = enc[:16]
        cipher = AES.new(self.key, AES.MODE_CBC, iv )
        return unpad(cipher.decrypt( enc[16:] ))

