import requests
import json
import pytz
import dateutil.parser #pip install python-dateutil
from dateutil.relativedelta import relativedelta, MO
from datetime import datetime,timedelta, timezone, tzinfo
import Cipher
import sys

cipher = Cipher.AESCipher(sys.argv[1])
def authenticate():
    response = requests.post('http://tms.routegiant.com/auth/local', json={"email":"lachlan.macpherson@routemonkey.com","password":cipher.decrypt("lKOzUllqQTQ9Rhr4wbdRN3E6mRfDV+mu2y1fNe26dZk=").decode("utf-8")})
    json_data = json.loads(response.text)
    return {'Authorization' : "bearer " + json_data["token"]}

def recordsToUpdate(weekCommenceDate, headers):
    payload = {'conditions': '{"archived":{"$ne":true}, "weekCommenceDate":{"$eq":"' + weekCommenceDate.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z' + '"}}', 'sort': 'endDate' }
    response = requests.get('http://tms.routegiant.com/api/tasks', params=payload, headers=headers)
    assert (response.status_code == 200)
    return json.loads(response.text)

def performUpdate(json_data, last_monday, next_monday ):  
    count = 0
    for record in json_data:
        str_val = record['weekCommenceDate']
        if str_val is not None:
            date_record = dateutil.parser.parse(str_val).strftime('%Y-%m-%d %H:%M:%S')
            if (date_record == last_monday.strftime('%Y-%m-%d %H:%M:%S')):
                print("Record {0} to be updated to {1}:...".format(record['jobNumber'], next_monday.strftime('%d-%m-%Y')), end='')
                record['weekCommenceDate'] = next_monday.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z'
                response = requests.put('http://tms.routegiant.com/api/tasks/' + record['_id'], headers=headers, json=record)
                print("Done.", flush=True)
                count+=1
    return count

def getLastNextMonday(today):
    ## If today is not day 5 or 6 e.g. Saturday or Sunday
    if (today.weekday() <= 4):
        last_monday = today + relativedelta(weekday=MO(-2))
        next_monday = today + relativedelta(weekday=MO(-1))
    else:
        last_monday = today + relativedelta(weekday=MO(-1))
        next_monday = today + relativedelta(weekday=MO(1))    
    last_monday = last_monday.replace(hour=0, minute=0, second=0, microsecond=0)
    next_monday = next_monday.replace(hour=0, minute=0, second=0, microsecond=0)
    return (last_monday, next_monday)


##debug for changing todays date.
##today = datetime.now() + timedelta(days=7)
today = datetime.now()
headers = authenticate()
lastNextMonday = getLastNextMonday(today)
json_data = recordsToUpdate(lastNextMonday[0], headers)
count = performUpdate(json_data, lastNextMonday[0], lastNextMonday[1])
print("Total {} Records updated".format(count))


