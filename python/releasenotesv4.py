# Remember, you need "set SVN_SSH="C:\\apps\\PuTTY\\plink.exe" in windows for SVN to work...

from subprocess import Popen, PIPE
import re
from restkit import Resource, BasicAuth, request, Connection
import simplejson as json
from socketpool import ConnectionPool
from natsort import natsort

class SvnSearch:

    def __init__(self, path):
       self.path = path

    def get_latest_revision(self):
        p = Popen('svn info %s' %(self.path),
          stdout=PIPE)
        stdout, stderr = p.communicate()
        regularExpression = re.compile('(Revision: )(\d+)')
        last_revision_kvp = regularExpression.findall(stdout)[0]
        return int(last_revision_kvp[1])

    def find_issues(self, start_revision, end_revision):
        #seems that in svn logs, : is being accidentally used rather than - for nmsproject key references. Convert : to - so nmsproject understands        
        p = Popen('svn log -v -r %d:%d %s' %(startrevision, endrevision, self.path),
          stdout=PIPE)
        stdout, stderr = p.communicate()
        regularExpression = re.compile('PSY[-:]\d+')
        keys = regularExpression.findall(stdout)
        return list(set(keys))
  
class JiraQuery:
    
    def __init__(self, username, password, server_url="https://nmsproject.grid.alstom.com/jira"):
        # This sends the user and password with the request.
        self.username = username
        self.password = password
        self.auth = BasicAuth(self.username, self.password)
        self.pool = ConnectionPool(factory=Connection)
        complete_url = "%s/rest/api/latest/search" % (server_base_url)
        self.resource = Resource(complete_url,pool=self.pool, filters=[self.auth])

    def search_issues(self, keys):
        try:            
            #seems that in svn logs, : is being accidentally used rather than - for nmsproject key references. Convert : to - so nmsproject understands        
            search_string = "key in (%s)" % (",".join(keys))
            search_string = re.sub(r"\s*:\s*", "-", search_string)
            
            data = { "jql": search_string, "fields": ["key", "summary", "status", "assignee", "issuetype"], "maxResults" : len(keys)}
            # do a POST since this is most efficient
            response = self.resource.post( payload = json.dumps( data ), headers = {'Content-Type' : 'application/json'} )    
        except Exception,ex:
            #ex.msg is a string that looks like a dictionary
            print "EXCEPTION: %s " % ex.msg
            return
        if response.status_int != 200:
            print "ERROR: status %s" % response.status_int
            return
        string = response.body_string()
        jqlReply = json.loads(string)
        return jqlReply['issues']   

#types = ['Story', 'Defect', 'Sub-task']
types = ['Story', 'Defect']    
path = "svn+ssh://lachlan@cypress.edi.psymetrix.com/data/repositories/pmx-msf/urtdsm/trunk"
project_name = path.rsplit('/', 1)[-1]
startrevision =  27798
endrevision = "HEAD"
server_base_url = "https://nmsproject.grid.alstom.com/jira"
username = "lachlan.macpherson"
password = ""


svn_search = SvnSearch(path)
if (endrevision == 'HEAD'):
   endrevision = svn_search.get_latest_revision()
keys = svn_search.find_issues(startrevision, endrevision)
jira_issues = []
if len(keys) > 0:
    jira_query = JiraQuery(username, password)
    jira_issues = jira_query.search_issues(keys)

file_name = 'release notes %s %s-%s.html' % (project_name, startrevision, endrevision)
with open(file_name, 'w') as myFile:
    myFile.write('<html>')
    myFile.write('<body>')
    myFile.write('<h1>Release Notes for %s</h1>' % (path))
    myFile.write('<h2>Revisions: (%s to %s)</h2>' % (startrevision, endrevision))

    for typeName in types:
        filteredSet = filter(lambda x: x['fields']['issuetype']['name'] == typeName, jira_issues)
    
        myFile.write('<h3>%s Set</h3>' % (typeName))
        myFile.write('<table>')
        myFile.write('<tr><th>Key</th><th>Summary</th></tr>')
        for issue in natsort(filteredSet, key=lambda x: x['key']):
            myFile.write('<tr>')
            print ("Writing %s %s" %(typeName, issue['key']))
            result = '<td><a href=\"%s/browse/%s">%s</a></td><td>%s</td>\n' %(server_base_url, issue['key'], issue['key'], issue['fields']['summary'])
            myFile.write(result.encode('UTF-8'))
            myFile.write('</tr>')
        myFile.write('</table>')
    
    myFile.write('</body>')
    myFile.write('</html>')
    print ("Written file \"%s\"" % (file_name))



