# -*- coding: utf-8 -*-
import requests
import json
import pytz
import dateutil.parser #pip install python-dateutil
import codecs
from dateutil.relativedelta import relativedelta, MO
from datetime import datetime,timedelta, timezone, tzinfo
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import Cipher
import sys

#notice trailing space for Osama
#name = "Osama "
name = "Grant"
ROOT_PATH = "http://tms.routegiant.com"
SEND_EMAILS = False



STAFF_EMAIL = {
    "Grant" : True,
    "Osama " : False,
}

table_style = """
    <style type="text/css">
    #workload {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    #workload td, #workload th {
        border: 1px solid #ddd;
        vertical-align: top;
        padding: 8px;
    }

    #description {
        width: 200px;
    }
    #workload tr:nth-child(even){background-color: #f2f2f2;}

    #workload tr:hover {background-color: #ddd;}

    #workload th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #4CAF50;
        color: white;
    }

  @import url('http://fonts.googleapis.com/css?family=Open+Sans:400,600,700');
 
   *, *:before, *:after {margin: 0; padding: 0; box-sizing: border-box;}
 
   h1 {padding: 100px 0; font-weight: 400; text-align: center;}
   p {margin: 0 0 20px; line-height: 1.5;}
 
   .main {margin: 0 auto; min-width: 320px; max-width: 1000px;}
   .content {background: #fff; color: #373737;}
   .content > div {display: none; padding: 20px 25px 5px;}
 
   input {display: none;}
   label {display: inline-block; padding: 15px 25px; font-weight: 600; text-align: center;background: #ed5a6a;}
   label:hover {color: #000; cursor: pointer;}
   input:checked + label {background: #ed5a6a; color: #fff;}
 
   #tab1:checked ~ .content #content1,
   #tab2:checked ~ .content #content2,
   #tab3:checked ~ .content #content3,
   #tab4:checked ~ .content #content4 {
     display: block;
   }
 
   @media screen and (max-width: 400px) { label {padding: 15px 10px;} }    
    </style>"""
class Mailer():

    def __init__(self, email, passwd):
        self.s = smtplib.SMTP('smtp.office365.com', 587)
        self.email = email
        self.passwd = passwd

    def __enter__(self):
        self.s.ehlo()
        self.s.starttls()
        self.s.login('lachlan.macpherson@routemonkey.com', self.passwd)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.s.quit()    
    
    def sendEmail(self, sender,usermail, user, date, content):
        msg = MIMEMultipart('alternative')
        msg['Subject'] = "{}'s Workload for Week {}".format(user, date.strftime('%d-%b-%Y'))
        msg['From'] = sender
        recipients = [usermail, sender]
        msg['To'] = ", ".join(recipients)
        part1 = MIMEText(content, 'html')
        msg.attach(part1)
        
        # sendmail function takes 3 arguments: sender's address, recipient's address
        # and message to send - here it is sent as one string.
        try:
            self.s.sendmail(sender, recipients, msg.as_string())
        except smtplib.SMTPException:
            log.info("Error: Unable to send email")
        
class Workload():

    def __init__(self, name, passwrd, sortOrder):
        response = requests.post('http://tms.routegiant.com/auth/local', json={"email": name,"password":passwrd})
        json_data = json.loads(response.text)
        self.email = name
        self.authHeader =  {'Authorization' : "bearer " + json_data["token"]}
        self.sortOrder = sortOrder
        self.Customers = self.__getAllCustomers()
        self.Users = self.__getAllUsers()
        self.Statuses = self.__getAllStatuses()
        
    def getIdFromName(self, name):
        payload = {'conditions': '{"name":{"$eq":"' + name + '"}}'}
        response = requests.get('http://tms.routegiant.com/api/users',  headers=self.authHeader, params=payload)
        json_data = json.loads(response.text)
        return json_data[0]["_id"]

    def __getAllCustomers(self):
        customersMap = {}
        response = requests.get('http://tms.routegiant.com/api/customers',  headers=self.authHeader )
        json_data = json.loads(response.text)
        for record in json_data:
            customersMap[record["_id"]] = record["name"]
        return customersMap

    def __getAllUsers(self):
        usersMap = {}
        response = requests.get('http://tms.routegiant.com/api/users',  headers=self.authHeader )
        json_data = json.loads(response.text)
        for record in json_data:
            usersMap[record["_id"]] = (record["name"], record["email"])
        return usersMap


    def __getAllStatuses(self):
        statusMap = {}
        response = requests.get('http://tms.routegiant.com/api/statuses',  headers=self.authHeader )
        json_data = json.loads(response.text)
        for record in json_data:
            statusMap[record["_id"]] = record["name"]
        return statusMap        
    
##    def getCustomer(self, customerId):
##        response = requests.get('http://tms.routegiant.com/api/customers/' + customerId,  headers=self.authHeader )
##        json_data = json.loads(response.text)
##        return json_data["name"]
##
##    def getStatus(self, statusId):
##        response = requests.get('http://tms.routegiant.com/api/statuses/' + statusId,  headers=self.authHeader )
##        json_data = json.loads(response.text)
##        return json_data["name"]
##
##    def getStatusId(self, statusName):
##        payload = {'conditions': '{"name":{"$eq":"' + statusName + '"}}'}
##        response = requests.get('http://tms.routegiant.com/api/statuses' ,  headers=self.authHeader, params=payload)
##        json_data = json.loads(response.text)
##        return json_data[0]["_id"]

    def getWorkload(self, statusId, person_id, this_monday ):
        this_monday = this_monday.replace(hour=0, minute=0, second=0, microsecond=0)
        #payload = {'conditions': '{"briefing":{"$eq":"false"},"status":{"$eq":"' + statusId + '"},"user":{"$eq":"' + person_id + '"}, "weekCommenceDate":{"$eq":"' + this_monday.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z' + '"}}', 'sort': self.sortOrder}
        payload = {'conditions': '{"user":{"$eq":"' + person_id + '"}, "status":{"$ne":"' + statusId + '"}, "archived":{"$ne":true}}' , 'sort': self.sortOrder}
        response = requests.get('http://tms.routegiant.com/api/tasks',  headers=self.authHeader, params=payload)
        return json.loads(response.text)
        

    def workLoadJsonToHtml(self, inProgressId, json_data, this_monday, name):
        this_monday = this_monday.replace(hour=0, minute=0, second=0, microsecond=0)
        next_monday = this_monday + timedelta(days=7)
        header = """ <input id="tab1" type="radio" name="tabs" checked>
		  <label for="tab1">This Week</label>
	
		  <input id="tab2" type="radio" name="tabs">
		  <label for="tab2">Next Week</label>

		  <input id="tab3" type="radio" name="tabs">
		  <label for="tab3">All Tasks</label>
		  """

        bufferString = "<html>{}<Head><meta charset=\"UTF-8\"><Title>{}'s Workload for Week {}</Title></Head><Body><DIV class=\"main\">" \
                       "{}<div class=\"content\">" \
                       .format(table_style, name, this_monday.strftime('%d-%b-%Y'),header)
        
        tableHeader = "<table id=\"workload\"><tr><th>Job</th><th width=100>Customer</th><th>Subject</th><th width=500>Summary</th><th>Due date</th><th>Duration</th><th>Status</th></tr>"
        tableFooter = "</table></div>"
        inProgressThisWeek = "<div id=\"content1\">" + "<H2>{}'s Workload for Week {}</H2>{}".format(name, this_monday.strftime('%d-%b-%Y'),tableHeader) 
        inProgressNextWeek = "<div id=\"content2\">" +  "<H2>{}'s Workload for Week {}</H2>{}".format(name, next_monday.strftime('%d-%b-%Y'), tableHeader)
        allTasks = "<div id=\"content3\"><H2>All Tasks</H2>" + tableHeader

        

        for record in json_data:
            due_date = record['dueDate']
            if due_date is not None:
                datestring = dateutil.parser.parse(due_date).strftime('%d-%b-%Y')
            else:
                datestring = ""
            secs = record['estDuration'] * 60
            hours = secs / 3600
            minutes = secs / 60 - hours * 60
            if ('dependency' not in record or record['dependency'] == "") and record['briefing'] == False and  record['status'] == inProgressId:
                if record['weekCommenceDate'] == this_monday.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z':        
                    inProgressThisWeek += "<tr><td><a target=\"_blank\" href=\"{}\">{}</a></td><td id=\"description\">{}</td><td>{}</td><td>{}</td><td>{}</td><td>{}</td><td>{}</td></tr>" \
                                  .format(ROOT_PATH + "/tasks/" + record['_id'] , record['jobNumber'], self.Customers[record['customer']],record['subject'], \
                                          convertToHTML(record['description']),   datestring, "%dh:%02dm" % (hours, minutes) , self.Statuses[record['status']])
                elif record['weekCommenceDate'] == next_monday.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z':        
                    inProgressNextWeek += "<tr><td><a target=\"_blank\" href=\"{}\">{}</a></td><td id=\"description\">{}</td><td>{}</td><td>{}</td><td>{}</td><td>{}</td><td>{}</td></tr>" \
                                  .format(ROOT_PATH + "/tasks/" + record['_id'] , record['jobNumber'], self.Customers[record['customer']],record['subject'], \
                                          convertToHTML(record['description']),   datestring, "%dh:%02dm" % (hours, minutes) , self.Statuses[record['status']])

            allTasks += "<tr><td><a target=\"_blank\" href=\"{}\">{}</a></td><td id=\"description\">{}</td><td>{}</td><td>{}</td><td>{}</td><td>{}</td><td>{}</td></tr>" \
                          .format(ROOT_PATH + "/tasks/" + record['_id'] , record['jobNumber'], self.Customers[record['customer']],record['subject'], \
                                  convertToHTML(record['description']),   datestring, "%dh:%02dm" % (hours, minutes) , self.Statuses[record['status']])
                

        inProgressThisWeek += tableFooter
        inProgressNextWeek += tableFooter
        allTasks += tableFooter

        bufferString += inProgressThisWeek
        bufferString += inProgressNextWeek
        bufferString += allTasks
        bufferString += "</HTML>"
        return bufferString

def convertToHTML(string_val):
    return string_val.replace('\n', '<BR/>')

def htmlOutput(stringtowrite, name ):
    filename='Workloads\\{} Workload.html'.format(name)
    fd = codecs.open(filename, "w", "utf-8")
    fd.write(stringtowrite)
    fd.close()

    #fd = open(filename, 'w')
    #fd.write(stringtowrite)
    #fd.close()

def main():
    cipher = Cipher.AESCipher(sys.argv[1])
    workload = Workload("lachlan.macpherson@routemonkey.com", cipher.decrypt("lKOzUllqQTQ9Rhr4wbdRN3E6mRfDV+mu2y1fNe26dZk=").decode("utf-8"), "_id")
    inProgressId =  [id for id, itername in workload.Statuses.items() if itername == "In Progress"][0]
    completeId =  [id for id, itername in workload.Statuses.items() if itername == "Complete"][0]
    #nameId = [id for id, itername in workload.Users.items() if itername == name][0]

    # Send the message via local SMTP server.
    today = datetime.now()
    ## If today is not day 5 or 6 e.g. Saturday or Sunday
    if (today.weekday() <= 4):
        this_monday = today + relativedelta(weekday=MO(-1))
    else:
        this_monday = today + relativedelta(weekday=MO(1))

    with Mailer('lachlan.macpherson@routemonkey.com', cipher.decrypt('TgiQ6dO6cF+Kc5wbeWMCvcod0t2sKxrCH0uwX8ZfszQ=').decode("utf-8")) as mailer:       
        for (nameId, (itername, email)) in sorted(workload.Users.items(), key=lambda k: k[1], reverse=False):
            print ("Workload for {}...".format(itername), end='', flush=True)
            workloadJson = workload.getWorkload(completeId, nameId, this_monday)
            sortedByCustomer = sorted(workloadJson, key=lambda k: workload.Customers[k["customer"]], reverse=False)
            html = workload.workLoadJsonToHtml(inProgressId, sortedByCustomer, this_monday, itername)
            if itername in STAFF_EMAIL and STAFF_EMAIL[itername] and SEND_EMAILS:
                print("Sending email...", end='')
                print (email)
                mailer.sendEmail(workload.email, email, itername, this_monday, html)
            htmlOutput(html, itername)
            print ("Done.", flush=True)

if __name__ == '__main__':
    main()



