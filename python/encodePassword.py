import Cipher
import sys

def main(cmd_args):
    cypher = Cipher.AESCipher(cmd_args[1])
    encrypted = cypher.encrypt(cmd_args[2])
    print (encrypted)
    decrypted = cypher.decrypt(encrypted)
    print (decrypted)
    
if __name__ == '__main__':
    main(sys.argv)
