#!/usr/bin/bash
#equivalent to a map...
#declare -A animals=( ["moo"]="cow" ["woof"]="dog")

currDir="$PWD"
for f in *; do
    if [[ -d $f ]]; then
		if [ "$f" == "Osama" ] || [ "$f" == "Thomas" ] ; then
			# $f is a directory
			for internal in $currDir/$f/*; do
				if [[ -d $internal ]]; then
					mkdir "${internal}_git"
					git init "${internal}_git"
					cd "${internal}_git"
					/c/Source/fast-export/fast-export/hg-fast-export.sh -r ${internal} --force
					git checkout HEAD
					echo "$internal"
				fi
			done
		fi
    fi
done

