package main

import (
	"fmt"
	"log"
	"io/ioutil"
	"net/http"
	"encoding/json"
	"strings"
	"os"
	"os/exec"
	"runtime"
	"time"
	"bufio"
  "html/template"
)

type Country struct {
	Country string
	City string
	Flag template.URL
}

type CountryFlag struct {
	Country string
	Flag_base64 string
}

var countries []Country
var countryFlags []CountryFlag
var clear map[string]func() //create a map for storing clear funcs

func init() {
    clear = make(map[string]func()) //Initialize it
    clear["linux"] = func() {
        cmd := exec.Command("clear") //Linux example, its tested
        cmd.Stdout = os.Stdout
        cmd.Run()
    }
    clear["windows"] = func() {
        cmd := exec.Command("cmd", "/c", "cls") //Windows example, its tested
        cmd.Stdout = os.Stdout
        cmd.Run()
    }
		clear["darwin"] = func() {
				cmd := exec.Command("clear scr") //Windows example, its tested
				cmd.Stdout = os.Stdout
				cmd.Run()
		}
}

func CallClear() {
    value, ok := clear[runtime.GOOS] //runtime.GOOS -> linux, windows, darwin etc.
		if ok { //if we defined a clear func for that platform:
        value()  //we execute it
    } else { //unsupported platform
        panic("Your platform is unsupported! I can't clear terminal screen :(")
    }
}


func GetCountryDetailsFromFile() []Country{
	// Open our jsonFile
	jsonFile, err := os.Open("countries.json")
	// if we os.Open returns an error then handle it
	if err != nil {
	    fmt.Println(err)
	}
	fmt.Println("Successfully Opened")
	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()
	byteValue, _ := ioutil.ReadAll(jsonFile)

	json.Unmarshal(byteValue, &countries)
	return countries
}

func GetCountryFlagsFromFile() []CountryFlag{
	// Open our jsonFile
	jsonFile, err := os.Open("flags.json")
	// if we os.Open returns an error then handle it
	if err != nil {
	    fmt.Println(err)
	}
	fmt.Println("Successfully Opened")
	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()
	byteValue, _ := ioutil.ReadAll(jsonFile)

	json.Unmarshal(byteValue, &countryFlags)
	return countryFlags
}

func GetCountryDetails() []Country{
		url:=`https://raw.githubusercontent.com/samayo/country-json/master/src/country-by-capital-city.json`

		req, err := http.NewRequest("GET", url, nil)
		req.Header.Set("Content-Type", "application/json")
		client := &http.Client{}
    resp, err := client.Do(req)
    if err != nil {
        panic(err)
    }
		defer resp.Body.Close()
		json.NewDecoder(resp.Body).Decode(&countries)
		return countries
}

func GetCountryFlags() []CountryFlag{
		url:=`https://raw.githubusercontent.com/samayo/country-json/master/src/country-by-flag.json`

		req, err := http.NewRequest("GET", url, nil)
		req.Header.Set("Content-Type", "application/json")
		client := &http.Client{}
    resp, err := client.Do(req)
    if err != nil {
        panic(err)
    }
		defer resp.Body.Close()
		json.NewDecoder(resp.Body).Decode(&countryFlags)
		return countryFlags
}

func CamelCase(str string) string{
	var g []string

	p := strings.Fields(str)

	for _,value := range p {
		g = append(g,strings.Title(value))
	}
	return strings.Join(g," ")
}

func GetCapitalAPI(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.Error(w, "404 not found.", http.StatusNotFound)
		return
	}

	switch r.Method {
	case "GET":
		 http.ServeFile(w, r, "form.html")
	case "POST":
		// Call ParseForm() to parse the raw query and update r.PostForm and r.Form.
		if err := r.ParseForm(); err != nil {
			fmt.Fprintf(w, "ParseForm() err: %v", err)
			return
		}
		//fmt.Fprintf(w, "Post from website! r.PostFrom = %v\n", r.PostForm)
		name := r.FormValue("name")
		cityIn := r.FormValue("city")
		if (strings.TrimSpace(cityIn) != ""){
		   nameIn := ReturnCountry(strings.TrimSpace(strings.ToLower(cityIn)))
			 if (nameIn != ""){
				 name = nameIn
			 }
		}
		tmplt := template.New("IndexTemplated.html")       //create a new template with some name
  	tmplt, _ = tmplt.ParseFiles("IndexTemplated.html") //parse some content and generate a template, which is an internal representation
    nme_out := CamelCase(name)
		flag := ReturnFlag(strings.TrimSpace(strings.ToLower(name)))
		yourhref := template.URL(flag)

		// Base64 Standard Decoding <-this is the part that was missing!!
		v := Country{Country:nme_out,City:CamelCase(ReturnCity(strings.TrimSpace(strings.ToLower(name)))), Flag:yourhref}
		tmplt.Execute(w, v) //merge template ‘t’ with content of ‘p’
		//fmt.Fprintf(w, "The capital of %v is %v", name, ReturnCity(strings.ToLower(name)))
	default:
		fmt.Fprintf(w, "Sorry, only GET and POST methods are supported.")
	}
}

func ReturnCountry(city_name string) string{
	var country_result string
	for _,country_item := range countries {
		 if (strings.ToLower(country_item.City) == strings.ToLower(city_name)){
			  country_result = country_item.Country
				break
		 }
	}
	return country_result
}

func ReturnFlag(country_name string) string{
	var flag_result string
	for _,country_item := range countryFlags {
		 if (strings.ToLower(country_item.Country) == strings.ToLower(country_name)){
			  flag_result = country_item.Flag_base64
				break
		 }
	}
	return flag_result
}

func ReturnCity(city_name string) string{
	var city_result string
	for _,country_item := range countries {
		 if (strings.ToLower(country_item.Country) == strings.ToLower(city_name)){
			  city_result = country_item.City
				break
		 }
	}
	return city_result
}

func UserInput(countries []Country) {
	var first string
	fmt.Println("\033[2J")

	for
	{
		time.Sleep(1 * time.Second)
		fmt.Println("Enter the country: ")
		reader := bufio.NewReader(os.Stdin)
		// ReadString will block until the delimiter is entered
		input, err := reader.ReadString('\n')
		if err != nil {
			fmt.Println("An error occured while reading input. Please try again", err)
			return
		}
		first = strings.TrimSuffix(input, "\n")
		if (strings.ToLower(first) == `x`){
			break
		}
		result := ReturnCity(first)
		fmt.Println("\033[2J")
		fmt.Printf("\nThe capital of %s is %s\n", first, result)
	}
}

func main() {
	  GetCountryDetailsFromFile()
		GetCountryFlagsFromFile()
		http.HandleFunc("/", GetCapitalAPI)
		argsWithoutProg := os.Args[1:]

		port := "9090"
    if (len(argsWithoutProg) > 0 && argsWithoutProg[0] != ""){
		   port = argsWithoutProg[0]
		}
		fmt.Printf("Starting server for testing HTTP POST...\n")
		if err := http.ListenAndServe(":" + port, nil); err != nil {
			log.Fatal(err)
		}
		//UserInput(countries)
}
