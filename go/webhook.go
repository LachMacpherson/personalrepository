package main

import (
        "fmt"
        "log"
        "os"
        "os/exec"
        "encoding/json"
        "net/http"
)

//const VALIDATE = "a59fc10f3c3f61510c95f522a441d447"
func check(e error) {
    if e != nil {
        panic(e)
    }
}

func writeFile(factPath string, key string, version string){
   formattedOutput := fmt.Sprintf("#!/bin/bash\necho \"%s=%s\"\n", key, version)
   f, err := os.OpenFile(factPath, os.O_TRUNC | os.O_WRONLY, 0644)
   check(err)
   defer f.Close()
   n3, err := f.WriteString(formattedOutput)
   check(err)
   fmt.Printf("wrote %d bytes\n", n3)
}


func handleWebhook(w http.ResponseWriter, r *http.Request, factPath1 string, factKey1, factPath2 string, factKey2, puppetPath string, validateString string) {
        webhookData := make(map[string]interface{})
        err := json.NewDecoder(r.Body).Decode(&webhookData)
        if err != nil || webhookData["token"] != validateString{
                http.Error(w, err.Error(), http.StatusInternalServerError)
                return
        }
        fmt.Println("got webhook payload: ")
        for k, v := range webhookData {
                fmt.Printf("%s : %v\n", k, v)
        }

        go writeFile(factPath1, factKey1, webhookData["version"].(string))
        go writeFile(factPath2, factKey2, "true")
        go runCommand(puppetPath)
        w.WriteHeader(http.StatusOK)
        w.Write([]byte("That worked!"))

}

func runCommand(puppetPath string){
   out,_ := exec.Command(puppetPath + "/puppet", "agent", "-t").CombinedOutput()
   fmt.Printf("%s\n", string(out))
}

func main() {
    argsWithoutProg := os.Args[1:]
    log.Printf("server started on %s", argsWithoutProg[0])
    http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
    handleWebhook(w, r, argsWithoutProg[1], argsWithoutProg[2], argsWithoutProg[3], argsWithoutProg[4], argsWithoutProg[5], argsWithoutProg[6])
})
    log.Fatal(http.ListenAndServe(":" + argsWithoutProg[0], nil))
}
