#!/bin/bash

export PATH="/usr/local/opt/openssl/bin:$PATH"

CERT_DAYS=3650
COMN_SUBJ="/C=Uk/ST=Scotland/L=Edinburgh/O=N-able PoC/OU=Engineering"

ROOT_NAME='RootCA'
CLNT_NAME='my_client'
SRVR_NAME='my_server'
TRST_NAME='truststore'

ROOT_DNS='ROOT'
CLNT_DNS='api-gateway.networkmacpherson.com'
SRVR_DNS="${1:-api-gateway.networkmacpherson.com}"

mkdir -p certs
cd certs

osslGen() { # fname, cmd, args...
    local fname="$1"
    local cmd="$2"
    shift 2
    [ -f "$fname" ] || openssl "$cmd" -out "$fname" "$@"
}

# Create the certificate authority
osslGen $ROOT_NAME.key genrsa 4096
osslGen $ROOT_NAME.pem req -new -x509 -days $CERT_DAYS -key $ROOT_NAME.key -subj "$COMN_SUBJ/CN=$ROOT_DNS"

osslGen $CLNT_NAME.key genrsa 2048
osslGen $CLNT_NAME.csr req -new -key $CLNT_NAME.key -addext "subjectAltName=DNS:$CLNT_DNS" -subj "$COMN_SUBJ/CN=$CLNT_DNS"
osslGen $CLNT_NAME.pem x509 -req -in $CLNT_NAME.csr -CA $ROOT_NAME.pem -CAkey $ROOT_NAME.key -set_serial 01 -days $CERT_DAYS -sha256 -extfile <(printf '%s' "subjectAltName=DNS:$CLNT_DNS")

osslGen $SRVR_NAME.key genrsa 2048
osslGen $SRVR_NAME.csr req -new -key $SRVR_NAME.key -addext "subjectAltName=DNS:$SRVR_DNS" -subj "$COMN_SUBJ/CN=$SRVR_DNS"
osslGen $SRVR_NAME.pem x509 -req -in $SRVR_NAME.csr -CA $ROOT_NAME.pem -CAkey $ROOT_NAME.key -set_serial 01 -days $CERT_DAYS -sha256 -extfile <(printf '%s' "subjectAltName=DNS:$SRVR_DNS")


cp $ROOT_NAME.pem $TRST_NAME.pem

echo "Remember to deploy your API Gateway after this is set"
