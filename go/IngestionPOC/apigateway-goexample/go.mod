module apigateway.com

go 1.17

require google.golang.org/protobuf v1.28.0

require (
	github.com/golang/protobuf v1.5.0 // indirect
	github.com/logicnow/msp-event-schema-gen-go v0.64.57
)

//replace github.com => ./ncgenerated/github.com
