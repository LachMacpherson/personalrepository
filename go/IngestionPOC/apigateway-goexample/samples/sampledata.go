package samples

import (
 	"google.golang.org/protobuf/types/known/timestamppb"  //Timestamp config
	"math"
	nc_entity "github.com/logicnow/msp-event-schema-gen-go/msp/nc/entity"
	nc_tasks "github.com/logicnow/msp-event-schema-gen-go/msp/nc/tasks"
  nc_asset"github.com/logicnow/msp-event-schema-gen-go/msp/nc/asset"
	nc "github.com/logicnow/msp-event-schema-gen-go/msp/nc"
)

type SampleData struct{}

func (sample *SampleData) GetDeviceSample() (*nc_entity.Device, string){
  device := nc_entity.Device{
    Context: getMspContextSample(),
    Action: nc.Action_EDIT,
    Deleted: false,
    ClientId: 101,
    DeviceId: 1907480434,
    Guid: "ee507d9d-498a-4921-a1ce-013a38504213-20190129-184159",
    Name: "LACHLAN-DT",
    Description: "Network device discovered using Asset Discovery - 1907480434",
    Uri: "1.1.1.1",
    DeviceClass: "Servers - Windows",
    IsManaged: false,
    Active: true,
    InstallTimestamp: timestamppb.Now(),
    WarrantyExpiryDate: timestamppb.Now(),
    PurchaseDate: timestamppb.Now(),
    SourceUri: "test.testing.com",
    RemoteControlUri: "remote.control.com",
    IsDynamicUri: true,
    IsSystem: true,
    IsProbe: true,
    IsDiscoveredAsset: true,
    LeaseExpiryDate: timestamppb.Now(),
    ExpectedReplacementDate: timestamppb.Now(),
    ExplicitlyUnmanaged: true,
    DeviceCost: 123.45,
    LastUpdated: timestamppb.Now(),
  }
  return &device,"Device"
}

func (sample *SampleData) GetClientSample() (*nc_entity.Client, string) {
  client := nc_entity.Client{
    Context: getMspContextSample(),
    Action: nc.Action_EDIT,
    Deleted: false,
    ClientId: 101,
    ParentId: 100,
    ClientType: nc_entity.Client_CUSTOMER,
    Created: timestamppb.Now(),
    LastUpdated: timestamppb.Now(),
    Address: []*nc_entity.Address{&nc_entity.Address{Address1: "235 street streen", Address2: "Lothian Road", City: "Edinburgh", PostalCode: "EH3 2DF", StateProvince: "Lothian", Country: "UK"}},
    Contact: []*nc_entity.Contact{&nc_entity.Contact{Title: "Mr", PhoneNumber: "123-123-5544", LastName: "Smith", FirstName: "John", Email: "me@me.com", Department: "My Department", IsPrimary: true, FullName: "John Robert Smith"}},
  }
  return &client, "Client"
}

func (sample *SampleData) GetApplianceSample() (*nc_asset.Appliance, string){
  appliance := nc_asset.Appliance{
    Context: getMspContextSample(),
    Action: nc.Action_EDIT,
    Deleted: false,
    LastUpdated: timestamppb.Now(),
    ApplianceId: 2013339320,
    ApplianceName: "2016-DUS - Windows",
    CustomerId: 252,
    DeviceId: 1938801289,
    ApplianceType: "Windows",
    IsConfigRequired: true,
    IsReloadRequired: false,
    IsPublic: false,
    Uri: "10.219.73.67",
    Description: "Windows Probe",
    Version: "2020.1.3.381",
    LastLogin: timestamppb.Now(),
    AutoUpdate: "Always",
    IsSystem: true,
    UpgradeAttempts: 6840,
    Reboot: false,
    CreationTime: timestamppb.Now(),
    IsModuleConfigRequired: true,
    ReportedTimezoneId: "02:00:00",
  }
  return &appliance,"Appliance"
}

func (sample *SampleData) GetGenericServiceData() (*nc_tasks.GenericServiceData, string){

  genericData := nc_tasks.GenericServiceData{
    Context: getMspContextSample(),
    Action: nc.Action_ADD,
    Deleted: false,
    LastUpdated: timestamppb.Now(),
    TaskId: 65573966,
    SourceName: "datalocalip_detailed",
    GenericField: getGenericFields(),
    ScanTime: timestamppb.Now(),
    StateId: 3,
  }

  return &genericData, "Generic"
}


//Private Methods
func getMspContextSample() *nc.MspContext{
  context := nc.MspContext{
    BizAppsCustomerId: "40C2FC73-33DD-E311-ABFA-E61F13ED92A1",
    SystemGuid: "c3ce19e7-155e-4460-8c68-e2275d7f97ba",
  }
  return &context
}

func getGenericFields() []*nc_tasks.GenericServiceDataGenericTypedData{
    var fields []*nc_tasks.GenericServiceDataGenericTypedData

    fields = append(fields, &nc_tasks.GenericServiceDataGenericTypedData{
      OneValue: &nc_tasks.GenericServiceDataGenericTypedData_IntegerField{IntegerField: 0},
      FieldName: "dns_response",
      FieldType: "Integer",
    })

    fields = append(fields, &nc_tasks.GenericServiceDataGenericTypedData{
      OneValue: &nc_tasks.GenericServiceDataGenericTypedData_BooleanField{BooleanField: false},
      FieldName: "dnsa",
      FieldType: "Boolean",
    })

    fields = append(fields, &nc_tasks.GenericServiceDataGenericTypedData{
      OneValue: &nc_tasks.GenericServiceDataGenericTypedData_BooleanField{BooleanField: true},
      FieldName: "dnsr",
      FieldType: "Boolean",
    })

    fields = append(fields, &nc_tasks.GenericServiceDataGenericTypedData{
      OneValue: &nc_tasks.GenericServiceDataGenericTypedData_IntegerField{IntegerField:  math.MaxInt32 - 1},
      FieldName: "foo01_tinyint",
      FieldType: "Integer",
    })

    fields = append(fields, &nc_tasks.GenericServiceDataGenericTypedData{
      OneValue: &nc_tasks.GenericServiceDataGenericTypedData_IntegerField{IntegerField:  math.MaxInt32 - 2},
      FieldName: "foo02_smallint",
      FieldType: "Integer",
    })

    fields = append(fields, &nc_tasks.GenericServiceDataGenericTypedData{
      OneValue: &nc_tasks.GenericServiceDataGenericTypedData_IntegerField{IntegerField:  math.MaxInt32 - 1},
      FieldName: "foo03_bigint",
      FieldType: "Long",
    })

    fields = append(fields, &nc_tasks.GenericServiceDataGenericTypedData{
      OneValue: &nc_tasks.GenericServiceDataGenericTypedData_FloatField{FloatField:  123.346},
      FieldName: "foo04_decimal",
      FieldType: "Float",
    })

    fields = append(fields, &nc_tasks.GenericServiceDataGenericTypedData{
      OneValue: &nc_tasks.GenericServiceDataGenericTypedData_FloatField{FloatField:  123.3467},
      FieldName: "foo05_decimal",
      FieldType: "Float",
    })

    fields = append(fields, &nc_tasks.GenericServiceDataGenericTypedData{
      OneValue: &nc_tasks.GenericServiceDataGenericTypedData_FloatField{FloatField:  123.34678},
      FieldName: "foo06_decimal",
      FieldType: "Float",
    })

    fields = append(fields, &nc_tasks.GenericServiceDataGenericTypedData{
      OneValue: &nc_tasks.GenericServiceDataGenericTypedData_DoubleField{DoubleField:  math.MaxFloat64 -1},
      FieldName: "foo07_double",
      FieldType: "Double",
    })


    fields = append(fields, &nc_tasks.GenericServiceDataGenericTypedData{
      OneValue: &nc_tasks.GenericServiceDataGenericTypedData_TimestampField{TimestampField:  timestamppb.Now()},
      FieldName: "foo08_date",
      FieldType: "Timestamp",
    })

    fields = append(fields, &nc_tasks.GenericServiceDataGenericTypedData{
      OneValue: &nc_tasks.GenericServiceDataGenericTypedData_TimestampField{TimestampField:  timestamppb.Now()},
      FieldName: "foo09_timestamp",
      FieldType: "Timestamp",
    })

    fields = append(fields, &nc_tasks.GenericServiceDataGenericTypedData{
      OneValue: &nc_tasks.GenericServiceDataGenericTypedData_StringField{StringField: "Some string value"},
      FieldName: "foo10_string",
      FieldType: "String",
    })
    return fields
}
