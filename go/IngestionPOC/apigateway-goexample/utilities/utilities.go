package utilities

import (
  "strings"
  "encoding/json"
  "encoding/base64"
)

func CamelCase(str string) string{
	var g []string

	p := strings.Fields(str)

	for _,value := range p {
		g = append(g,strings.Title(value))
	}
	return strings.Join(g," ")
}

func ConvertToLowerTrimmed(str string) string{
	return strings.TrimSpace(strings.ToLower(str))
}

func ConvertStringToJson(result []byte, v interface{}){
  	err := json.Unmarshal(result, v)
    if err != nil {
  			panic(err)
  	}
}

func DecodeB64(message string) (retour string) {
    base64Text := make([]byte, base64.StdEncoding.DecodedLen(len(message)))
    base64.StdEncoding.Decode(base64Text, []byte(message))
    return string(base64Text)
}
