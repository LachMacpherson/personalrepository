package main

import (
	"bytes"
	"compress/gzip"
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"reflect"
	"strings"
	"sync"
	"time"

	"apigateway.com/samples"
	"apigateway.com/utilities"
	"google.golang.org/protobuf/encoding/protojson"
	"google.golang.org/protobuf/proto"
)

type Record struct {
	Data interface{} `json:"data"`
}

type Records struct {
	Records []Record `json:"records"`
}

type PutRecordResponse struct {
	SequenceNumber string `json:"SequenceNumber"`
	ShardId        string `json:"ShardId"`
}

type PutRecordResponses struct {
	FailedRecordCount *int                `json:"integer,FailedRecordCount"`
	Records           []PutRecordResponse `json:"records"`
}

type ReqObject struct {
	stream  string
	payload string
}

const URL = "https://%s/%s"

const REQUESTS_COUNT = 100
const RECORD_COUNT = 10
const MESSAGES_PER_RECORD = 200

func randInt(min int, max int) int {
	rand.Seed(time.Now().UTC().UnixNano())
	return rand.Intn(30)
}

func compressJSON(myJSON []byte) (bytes.Buffer, error) {
	var buf bytes.Buffer
	gz := gzip.NewWriter(&buf)
	gz.Write(myJSON)
	return buf, gz.Close()
}

func BuildMessagePayload(noOfMessages int, message proto.Message) []string {
	var arr []string

	var myJson2 []byte

	for i := 0; i < noOfMessages; i++ {
		myJson2, _ = protojson.MarshalOptions{UseProtoNames: true, EmitUnpopulated: true}.Marshal(message)
		arr = append(arr, string(myJson2))
	}
	return arr
}

func MakePost(stream string, wg *sync.WaitGroup, recordCount int, client http.Client, apiDomainName string, apiKey string, payload string, ch chan<- string) {
	defer wg.Done()
	var records []Record
	var recordResponses PutRecordResponses
	for i := 0; i < recordCount; i++ {
		records = append(records, Record{
			Data: string(payload),
		})
	}
	myJson, _ := json.Marshal(Records{Records: records})
	buf, _ := compressJSON(myJson)
	req, err := http.NewRequest("PUT",  fmt.Sprintf(URL, apiDomainName,stream) , bytes.NewReader(buf.Bytes()))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Content-Encoding", "gzip")
	req.Header.Set("x-api-key", apiKey)
	start := time.Now()
	resp, err := client.Do(req)
	secs := time.Since(start).Seconds()
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode >= 200 && resp.StatusCode <= 299 {
		utilities.ConvertStringToJson([]byte(bodyBytes), &recordResponses)
		ch <- fmt.Sprintf("%d Messages should now be pushed to S3 in %.2f secs\n", (recordCount * MESSAGES_PER_RECORD), secs)
	} else {
		// Convert response body to string
		ch <- fmt.Sprintf("Error %d (%s) writing to Gateway.\n", resp.StatusCode, http.StatusText(resp.StatusCode))
	}
}

func main() {
	apiKey := flag.String("apikey", "", "API Key for API Gateway")
	apiDomainName := flag.String("apidomainname", "", "API domain name")
	flag.Parse()
	if *apiKey == "" {
		panic("You must enter -apikey <API Key> to run this application")
	}

	cert, err := ioutil.ReadFile("certs/RootCA.pem")
	if err != nil {
		log.Fatalf("could not open certificate file: %v", err)
	}
	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(cert)
	certificate, err := tls.LoadX509KeyPair("certs/my_client.pem", "certs/my_client.key")
	if err != nil {
		log.Fatalf("could not load certificate: %v", err)
	}

	client := http.Client{
		Timeout: time.Second * 10,
		Transport: &http.Transport{
			MaxIdleConnsPerHost: -1,
			DisableKeepAlives:   true,

			TLSClientConfig: &tls.Config{
				ClientAuth:   tls.RequireAndVerifyClientCert,
				RootCAs:      caCertPool,
				ClientCAs:    caCertPool,
				Certificates: []tls.Certificate{certificate},
			},
		},
	}

	var requests []ReqObject
	sampleData := samples.SampleData{}
	t := reflect.TypeOf(&sampleData)
	for i := 0; i < REQUESTS_COUNT; i++ {
		rand.Seed(time.Now().UnixNano())
		min := 1
		max := t.NumMethod()
		var message proto.Message
		var stream string
		randNo := rand.Intn(max-min+1) + min

		message, stream = sampleData.GetGenericServiceData()
		for z := 0; z < t.NumMethod(); z++ {
			if z == (randNo - 1) {
				result := reflect.ValueOf(&sampleData).MethodByName(t.Method(z).Name).Call([]reflect.Value{})
				message = result[0].Interface().(proto.Message)
				stream = result[1].Interface().(string)
				break
			}
		}

		requests = append(requests, ReqObject{stream: stream, payload: strings.Join(BuildMessagePayload(MESSAGES_PER_RECORD, message), "")})
	}

	ch := make(chan string, 100)
	start := time.Now()

	var wg sync.WaitGroup
	for _, obj := range requests {
		wg.Add(1)
		go MakePost(obj.stream, &wg, RECORD_COUNT, client, *apiDomainName, *apiKey, obj.payload, ch)
	}

	for range requests {
		fmt.Printf(<-ch)
	}
	wg.Wait()
	close(ch)
	secs := time.Since(start).Seconds()

	fmt.Printf("%d Messages written in %.2f secs\n", (REQUESTS_COUNT * RECORD_COUNT * MESSAGES_PER_RECORD), secs)
	fmt.Printf("This is %.2f messages/sec\n", (REQUESTS_COUNT*RECORD_COUNT*MESSAGES_PER_RECORD)/secs)
}
