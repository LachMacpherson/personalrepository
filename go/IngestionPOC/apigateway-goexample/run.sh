#!/bin/sh


if [[ -z "${GATEWAY_APIKEY}" ]]; then
  echo "GATEWAY_APIKEY Environment Variable must be set in order to use this App."
  exit 0
else
  go run apigateway.com -apidomainname "${1:-api-gateway.networkmacpherson.com}/streams" -apikey ${GATEWAY_APIKEY}
fi
