# IngestionPOC

Terraform init
https://n-able.atlassian.net/wiki/spaces/RI/pages/58439841319/Run+Terraform+deployment+scripts
use_anvil_aws sisense_dev-developer


terraform plan -var aws_access_key=${AWS_ACCESS_KEY_ID} -var aws_region=us-east-1 -var aws_secret_key=${AWS_SECRET_ACCESS_KEY} -var aws_session_token=${AWS_SESSION_TOKEN}
