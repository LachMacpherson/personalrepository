output "api_key" {
  value     = aws_api_gateway_api_key.rest_api_key.value
  sensitive = true
}

output "regional_domain_name" {
  value = aws_api_gateway_domain_name.rest_api_domain.regional_domain_name
}
