terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.6.0"
    }
  }
}

provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  token      = var.aws_session_token
  region     = var.aws_region
}

locals {
  name_suffix = "poc-bf8d6fc8-00ag"

  certs_dir = "${path.module}/../apigateway-goexample/certs"
  mtls = {
    private_key = "${local.certs_dir}/my_server.key"
    cert_body   = "${local.certs_dir}/my_server.pem"
    cert_chain  = "${local.certs_dir}/RootCA.pem"
    truststore  = "${local.certs_dir}/truststore.pem"
  }
}

resource "aws_s3_bucket" "s3_bucket" {
  bucket = "s3-bucket-${local.name_suffix}"
}

resource "aws_iam_role" "firehose_role" {
  name = "firehose-role-${local.name_suffix}"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Action = "sts:AssumeRole"
        Sid    = ""
        Principal = {
          Service = "firehose.amazonaws.com"
        }
      }
    ]
  })

  inline_policy {
    name = "firehose-role-policy-${local.name_suffix}"

    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Effect = "Allow"
          Action = [
            "s3:AbortMultipartUpload",
            "s3:GetBucketLocation",
            "s3:GetObject",
            "s3:ListBucket",
            "s3:ListBucketMultipartUploads",
            "s3:PutObject"
          ]
          Resource = [
            "${aws_s3_bucket.s3_bucket.arn}",
            "${aws_s3_bucket.s3_bucket.arn}/*"
          ]
        },
      ]
    })
  }
}

resource "aws_kinesis_firehose_delivery_stream" "firehose_s3_stream-client" {
  name        = "firehose-s3-stream-client-${local.name_suffix}"
  destination = "extended_s3"

  extended_s3_configuration {
    role_arn   = aws_iam_role.firehose_role.arn
    bucket_arn = aws_s3_bucket.s3_bucket.arn
    prefix             = "ncLoad/ClientData/!{timestamp:yyyy-MM-dd}/"
    error_output_prefix= "ncError"
    buffer_size        = 10
    buffer_interval    = 60
    compression_format = "GZIP"
  }
}

resource "aws_kinesis_firehose_delivery_stream" "firehose_s3_stream-device" {
  name        = "firehose-s3-stream-device-${local.name_suffix}"
  destination = "extended_s3"

  extended_s3_configuration {
    role_arn   = aws_iam_role.firehose_role.arn
    bucket_arn = aws_s3_bucket.s3_bucket.arn
    prefix             = "ncLoad/DeviceData/!{timestamp:yyyy-MM-dd}/"
    error_output_prefix= "ncError"
    buffer_size        = 10
    buffer_interval    = 60
    compression_format = "GZIP"
  }
}

resource "aws_kinesis_firehose_delivery_stream" "firehose_s3_stream-appliance" {
  name        = "firehose-s3-stream-appliance-${local.name_suffix}"
  destination = "extended_s3"

  extended_s3_configuration {
    role_arn   = aws_iam_role.firehose_role.arn
    bucket_arn = aws_s3_bucket.s3_bucket.arn
    prefix             = "ncLoad/ApplianceData/!{timestamp:yyyy-MM-dd}/"
    error_output_prefix= "ncError"
    buffer_size        = 10
    buffer_interval    = 60
    compression_format = "GZIP"
  }
}

resource "aws_kinesis_firehose_delivery_stream" "firehose_s3_stream-generic" {
  name        = "firehose-s3-stream-generic-${local.name_suffix}"
  destination = "extended_s3"

  extended_s3_configuration {
    role_arn   = aws_iam_role.firehose_role.arn
    bucket_arn = aws_s3_bucket.s3_bucket.arn
    prefix             = "ncLoad/GenericData/!{timestamp:yyyy-MM-dd}/"
    error_output_prefix= "ncError"
    buffer_size        = 10
    buffer_interval    = 60
    compression_format = "GZIP"
  }
}


resource "aws_api_gateway_rest_api" "rest_api" {
  name = "rest-api-${local.name_suffix}"
  disable_execute_api_endpoint = true
  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

resource "aws_api_gateway_resource" "rest_resource_root" {
  rest_api_id = aws_api_gateway_rest_api.rest_api.id
  parent_id   = aws_api_gateway_rest_api.rest_api.root_resource_id
  path_part   = "streams"
}


resource "aws_api_gateway_resource" "rest_resource" {
  rest_api_id = aws_api_gateway_rest_api.rest_api.id
  parent_id   = aws_api_gateway_resource.rest_resource_root.id
  path_part   = "{type-name}"
}

resource "aws_api_gateway_method" "rest_method" {
  rest_api_id      = aws_api_gateway_rest_api.rest_api.id
  resource_id      = aws_api_gateway_resource.rest_resource.id
  http_method      = "PUT"
  authorization    = "NONE"
  api_key_required = true
}

resource "aws_api_gateway_method_response" "response_200" {
  rest_api_id = aws_api_gateway_rest_api.rest_api.id
  resource_id = aws_api_gateway_resource.rest_resource.id
  http_method = aws_api_gateway_method.rest_method.http_method
  status_code = "200"

  depends_on = [
    aws_api_gateway_method.rest_method
  ]
}

resource "aws_iam_role" "gateway_role" {
  name = "gateway-role-${local.name_suffix}"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Action = "sts:AssumeRole"
        Sid    = ""
        Principal = {
          Service = "apigateway.amazonaws.com"
        }
      }
    ]
  })

  inline_policy {
    name = "gateway-role-policy-${local.name_suffix}"

    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Effect = "Allow"
          Action = [
            "firehose:PutRecordBatch",
          ]
          Resource = "*"
        },
      ]
    })
  }
}


resource "aws_api_gateway_integration" "firehose_integration" {
  rest_api_id = aws_api_gateway_rest_api.rest_api.id
  resource_id = aws_api_gateway_resource.rest_resource.id
  http_method = aws_api_gateway_method.rest_method.http_method
  type        = "AWS"

  credentials = aws_iam_role.gateway_role.arn

  integration_http_method = "POST"
  uri                     = "arn:aws:apigateway:${var.aws_region}:firehose:action/PutRecordBatch"
  passthrough_behavior    = "NEVER"

  request_templates = {
    "application/json" = <<-EOF
      {
          #set($testValue = "$input.params('type-name')")
          #if($testValue.equals("Client"))
              #set($streamName =  "${aws_kinesis_firehose_delivery_stream.firehose_s3_stream-client.name}")
          #elseif($testValue.equals("Device"))
              #set($streamName = "${aws_kinesis_firehose_delivery_stream.firehose_s3_stream-device.name}")
          #elseif($testValue.equals('Appliance'))
              #set($streamName = "${aws_kinesis_firehose_delivery_stream.firehose_s3_stream-appliance.name}")
          #else
              #set($streamName =  "${aws_kinesis_firehose_delivery_stream.firehose_s3_stream-generic.name}")
          #end
          "DeliveryStreamName": "$streamName",
          "Records": [
             #foreach($elem in $input.path('$.records'))
                {
                  "Data": "$util.base64Encode($elem.data)"
                }#if($foreach.hasNext),#end
              #end
          ]
      }
      EOF
  }

  request_parameters = {
    "integration.request.header.Content-Type" = "'application/x-amz-json-1.1'"
  }
}

resource "aws_api_gateway_integration_response" "firehose_integration_response" {
  rest_api_id = aws_api_gateway_rest_api.rest_api.id
  resource_id = aws_api_gateway_resource.rest_resource.id
  http_method = aws_api_gateway_method.rest_method.http_method
  status_code = aws_api_gateway_method_response.response_200.status_code

  response_templates = {
    "application/json" = jsonencode({
      status = "OK"
    })
  }

  depends_on = [
    aws_api_gateway_integration.firehose_integration
  ]
}

resource "aws_api_gateway_deployment" "rest_api_deploy" {
  rest_api_id = aws_api_gateway_rest_api.rest_api.id

  triggers = {
    redeployment = sha1(jsonencode(aws_api_gateway_rest_api.rest_api.body))
  }

  lifecycle {
    create_before_destroy = true
  }

  depends_on = [
    aws_api_gateway_method.rest_method,
    aws_api_gateway_integration.firehose_integration
  ]
}

resource "aws_api_gateway_stage" "production" {
  deployment_id = aws_api_gateway_deployment.rest_api_deploy.id
  rest_api_id   = aws_api_gateway_rest_api.rest_api.id
  stage_name    = "production-${local.name_suffix}"
}

resource "aws_api_gateway_usage_plan" "usage_plan" {
  name = "usage-plan-${local.name_suffix}"

  api_stages {
    api_id = aws_api_gateway_rest_api.rest_api.id
    stage  = aws_api_gateway_stage.production.stage_name
  }
}

resource "aws_api_gateway_api_key" "rest_api_key" {
  name = "rest-api-key-${local.name_suffix}"
}

resource "aws_api_gateway_usage_plan_key" "main" {
  usage_plan_id = aws_api_gateway_usage_plan.usage_plan.id
  key_id        = aws_api_gateway_api_key.rest_api_key.id
  key_type      = "API_KEY"
}

resource "aws_acm_certificate" "owner_cert" {
  domain_name       = var.api_domain_name
  validation_method = "EMAIL"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate_validation" "owner_cert" {
  certificate_arn = aws_acm_certificate.owner_cert.arn
}

resource "aws_acm_certificate" "mtls_cert" {
  private_key       = file(local.mtls.private_key)
  certificate_body  = file(local.mtls.cert_body)
  certificate_chain = file(local.mtls.cert_chain)
}

resource "aws_s3_bucket" "mtls_truststore" {
  bucket = "mtls-truststore-${local.name_suffix}"
}

resource "aws_s3_bucket_versioning" "mtls_truststore" {
  bucket = aws_s3_bucket.mtls_truststore.id

  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_object" "mtls_truststore" {
  bucket = aws_s3_bucket.mtls_truststore.bucket
  key    = "truststore.pem"
  source = local.mtls.truststore
  etag   = filemd5(local.mtls.truststore)

  depends_on = [
    aws_s3_bucket_versioning.mtls_truststore
  ]
}

resource "aws_api_gateway_domain_name" "rest_api_domain" {
  domain_name     = aws_acm_certificate.owner_cert.domain_name
  security_policy = "TLS_1_2"

  regional_certificate_arn               = aws_acm_certificate.mtls_cert.arn
  ownership_verification_certificate_arn = aws_acm_certificate_validation.owner_cert.certificate_arn

  endpoint_configuration {
    types = ["REGIONAL"]
  }

  mutual_tls_authentication {
    truststore_uri = "s3://${aws_s3_bucket.mtls_truststore.bucket}/${aws_s3_object.mtls_truststore.key}"
    # truststore_version = "Latest"
  }
}

resource "aws_api_gateway_base_path_mapping" "rest_api_mapping" {
  api_id      = aws_api_gateway_rest_api.rest_api.id
  stage_name  = aws_api_gateway_stage.production.stage_name
  domain_name = aws_api_gateway_domain_name.rest_api_domain.domain_name
}
