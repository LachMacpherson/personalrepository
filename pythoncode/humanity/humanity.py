import requests
import json
from datetime import datetime, timedelta as td, date
import iso8601
from pytz import timezone
localtz = timezone('Europe/London')
import os
from flask import Flask, session,make_response
from flask import render_template
from flask import request, Response
from functools import wraps
from collections import OrderedDict

app = Flask(__name__)
app.secret_key = os.urandom(24)


def check_auth(username, password):
    """This function is called to check if a username /
    password combination is valid.
    """
    return username == 'admin' and password == 's3cr3t!'

def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response(
    'Could not verify your access level for that URL.\n'
    'You have to login with proper credentials', 401,
    {'WWW-Authenticate': 'Basic realm="Login Required"'})

def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)
    return decorated

def authenticate_humanity():
    header = {"Content-Type": "application/x-www-form-urlencoded"}
    body =  {"data" : """{"key": "b13929e34a83b16985b75b1f2f6dea7a44292c98","request": {"module": "staff.login","method": "GET","username": "lachlan@textensor.com","password": "Lexus234"}}"""}
    response = requests.post('https://www.humanity.com/api/rest', data=body, headers=header)
    json_data = json.loads(response.text)
    return json_data["token"]

def getvacationschedule(token, startMonth, endMonth):
    start = date(date.today().year, int(startMonth), 1).strftime("%b %d, %Y")
    end = date(date.today().year, int(endMonth), 31).strftime("%b %d, %Y")
    header = {"Content-Type": "application/x-www-form-urlencoded"}
    body =  {"data" : """{"token": \"""" + token + """\","request": {"module": "schedule.vacations","method": "GET", "start_date" : \"""" +  start + """\", "end_date" : \"""" + end + """\"}}"""}
    response = requests.post('https://www.humanity.com/api/rest', data=body, headers=header)
    json_data = json.loads(response.text)
    return json_data["data"]

def buildCalendarData(json_data, startMonth, endMonth):
    a = date(date.today().year, int(startMonth), 1)
    b = date(date.today().year, int(endMonth), 31)
    delta = b - a
    monthVal = 0
    monthMap = {}
    for i in range(delta.days + 1):
        currdate= a + td(days=i)
        if (currdate.month > monthVal):
           monthMap[currdate.strftime("%B")] = []
           monthVal = monthVal + 1
        datedata = list(filter(lambda x: localtz.localize(datetime.combine(currdate, datetime.min.time())) >= iso8601.parse_date(x["start_day"]["iso8601"]) and localtz.localize(datetime.combine(currdate, datetime.min.time())) <=  iso8601.parse_date(x["end_day"]["iso8601"]), json_data))
        monthMap[currdate.strftime("%B")].append(((currdate.day, currdate.strftime("%a")) , datedata))
    return monthMap

@app.route('/')
@requires_auth
def index():
    startMonth = request.args.get('startMonth')
    endMonth = request.args.get('endMonth')
    if (startMonth is None):
        startMonth = date.today().month

    if (endMonth is None):
        endMonth = date.today().month

    token = authenticate_humanity()
    json_data = getvacationschedule(token, startMonth, endMonth)

    outOfOfficetoday = filter(lambda x: localtz.localize(datetime.now()) >= iso8601.parse_date(x["start_day"]["iso8601"]) and localtz.localize(datetime.now()) <=  iso8601.parse_date(x["until"]["iso8601"]) and x["leave_type_name"] == "Out of Office", json_data)
    vacationtoday = filter(lambda x: localtz.localize(datetime.now()) >= iso8601.parse_date(x["start_day"]["iso8601"]) and localtz.localize(datetime.now()) <=  iso8601.parse_date(x["until"]["iso8601"]) and x["leave_type_name"] == "Vacation", json_data)
    other = filter(lambda x: localtz.localize(datetime.now()) >= iso8601.parse_date(x["start_day"]["iso8601"]) and localtz.localize(datetime.now()) <=  iso8601.parse_date(x["until"]["iso8601"]) and x["leave_type_name"] != "Vacation" and x["leave_type_name"] != "Out of Office", json_data)
    dataMap = buildCalendarData(json_data, startMonth, endMonth)

    sortedDataMap = OrderedDict(sorted(dataMap.items(), key=lambda x:datetime.strptime(x[0], "%B") , reverse=False))
    return render_template('index.html', outoffice=outOfOfficetoday, vacation=vacationtoday, other=other, total=json_data, dataMap=sortedDataMap, currDay=datetime.now().strftime("%d"), currMonth=datetime.now().strftime("%B"))

@app.route('/slack')
def slack_event():
    slack_event = json.loads(request.data)

    # ============= Slack URL Verification ============ #
    # In order to verify the url of our endpoint, Slack will send a challenge
    # token in a request and check for this token in the response our endpoint
    # sends back.
    #       For more info: https://api.slack.com/events/url_verification
    if "challenge" in slack_event:
        return make_response(slack_event["challenge"], 200, {"content_type":
                                                             "application/json"
                                                             })
if __name__ == '__main__':
	app.run(
        host="0.0.0.0",
        port=int("82")
  )
